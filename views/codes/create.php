<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\codes */

$this->title = 'Sukurti kodą';
$this->params['breadcrumbs'][] = ['label' => 'Delegacijų kodai', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="codes-create">

 
    
    <?= $this->render('_form', [
        'model' => $model,'departments'=>$departments
    ]) ?>

</div>
