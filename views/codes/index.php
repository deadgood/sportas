<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Delegacijų kodai';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="codes-index">

  

    <p>
        <?= Html::a('Sukurti kodą', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'code',
            ['attribute' => 'department_id',

                'content' => function ($model) {
                   
                    $name = \app\models\Departments::find()->where(['id' => $model['department_id']])->one();
                    return $name['name'];
                },
                'label' => 'Bendruomenė',
                'enableSorting' => TRUE
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
