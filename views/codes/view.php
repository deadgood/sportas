<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Departments;

/* @var $this yii\web\View */
/* @var $model app\models\codes */
$name = Departments::find()->where(['id'=>$model->department_id])->one();
$this->title = $name['name']. ' kodas';

$this->params['breadcrumbs'][] = ['label' => 'Delegacijų kodai', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="codes-view">

 

    <p>
        <?= Html::a('Atnaujinti', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Trinti', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            [
                'label' => $model->getAttributeLabel('department_id'),
                'value' => !empty($model->department) ? $model->department->name : '-'

            ],

        ],
    ]) ?>

</div>
