<?php

use yii\helpers\Html;
use app\models\Departments;

/* @var $this yii\web\View */
/* @var $model app\models\codes */
$name = Departments::find()->where(['id'=>$model->department_id])->one();
$this->title = 'Atnaujinti kodą: ' . $name['name'];
$this->params['breadcrumbs'][] = ['label' => 'Delegacijų kodai', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $name['name'];
?>
<div class="codes-update">

   

    <?= $this->render('_form', [
        'model' => $model,'departments'=>$departments
    ]) ?>

</div>
