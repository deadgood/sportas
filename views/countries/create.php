<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\countries */

$this->title = 'Sukurti šalį';
$this->params['breadcrumbs'][] = ['label' => 'Šalys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="countries-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
