<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\countries */

$this->title = 'Šalis: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Šalys', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Atnaujinti';
?>
<div class="countries-update">

  

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
