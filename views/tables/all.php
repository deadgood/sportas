<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Dalyvių ataskaita';
$this->params['breadcrumbs'][] = 'Ataskaitos';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="tables-index">


    <?php

    echo ExportMenu::widget([
        'dataProvider' => $dataProviderOverall,
        'columns' => [

            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'Vardas',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->name)) {
                        return $model->name;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'Pavardė',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->surname)) {
                        return $model->surname;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'Šalis',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->county->country_name)) {
                        return $model->county->country_name;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'Delegacija',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->department->name)) {
                        return $model->department->name;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'club',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->club)) {
                        return $model->club;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'Lytis',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->sex->name)) {
                        return $model->sex->name;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'Amžius',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->age)) {
                        return $model->age;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'Akreditacija1',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->accreditation_first)) {
                        return $model->accreditation_first;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'accreditation_second',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->accreditation_second)) {
                        return $model->accreditation_second;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'scope_first',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->scope_first)) {
                        return $model->scope_first;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'scope_second',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->scope_second)) {
                        return $model->scope_second;
                    } else {
                        return '';
                    }
                }

            ],

            [
                'attribute' => 'scope_third',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->scope_third)) {
                        return $model->scope_third;
                    } else {
                        return '';
                    }
                }
            ],

            [
                'attribute' => 'bed_from',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->bed_from)) {
                        return $model->bed_from;
                    } else {
                        return '';
                    }
                }
            ],

            [
                'attribute' => 'bed_to',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->bed_to)) {
                        return $model->bed_to;
                    } else {
                        return '';
                    }
                }
            ],

            [
                'attribute' => 'transport_id',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->transport->name)) {
                        return $model->transport->name;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'capitals_id',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->capitals->name)) {
                        return $model->capitals->name;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'arrival',

                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->arrival)) {
                        return $model->arrival;
                    } else {
                        return '';
                    }

                },

            ],

            [
                'attribute' => 'off_transport_id',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->offTransport->name)) {
                        return $model->offTransport->name;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'off_capital_id',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->offCapital->name)) {
                        return $model->offCapital->name;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'departue',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->departue)) {
                        return $model->departue;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'responsible_phone',
                'value' => function ($model, $key, $index, $widget) use ($responsible_array) {
                    if (!empty($model->responsible_phone)) {
                        return $model->responsible_phone;
                    } else {
                        if(isset($responsible_array[$model->department_id]['responsible_phone'])) {
                            return $responsible_array[$model->department_id]['responsible_phone'];
                        }else{
                            return '';
                        }
                    }

                },
            ],

            [
                'attribute' => 'responsible_mail',
                'value' => function ($model, $key, $index, $widget) use ($responsible_array) {
                    if (!empty($model->responsible_mail)) {
                        return $model->responsible_mail;
                    } else {
                        if(isset($responsible_array[$model->department_id]['responsible_mail'])) {
                            return $responsible_array[$model->department_id]['responsible_mail'];
                        }else{
                            return '';
                        }
                    }
                },
            ],

            [
                'attribute' => 'specials',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->specials)) {
                        return $model->specials;
                    } else {
                        return '';
                    }

                },
            ],
        ],
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-default'
        ]
    ]);


    echo GridView::widget([
        'dataProvider' => $dataProviderOverall,
        'striped' => true,
        'hover' => true,
        'panel' => ['type' => 'primary', 'heading' => 'Bendra ataskaita'],
        'responsive' => true,
        'export' => false,
        'columns' => [

            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'Vardas',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->name)) {
                        return $model->name;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'Pavardė',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->surname)) {
                        return $model->surname;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'Šalis',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->county->country_name)) {
                        return $model->county->country_name;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'Delegacija',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->department->name)) {
                        return $model->department->name;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'Klubas',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->club)) {
                        return $model->club;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'Lytis',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->sex->name)) {
                        return $model->sex->name;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'Amžius',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->age)) {
                        return $model->age;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'Akreditacija1',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->accreditation_first)) {
                        return $model->accreditation_first;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'Akreditacija2',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->accreditation_second)) {
                        return $model->accreditation_second;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'Sporto šaka1',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->scope_first)) {
                        return $model->scope_first;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'Sporto šaka2',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->scope_second)) {
                        return $model->scope_second;
                    } else {
                        return '';
                    }
                }

            ],

            [
                'attribute' => 'Sporto šaka3',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->scope_third)) {
                        return $model->scope_third;
                    } else {
                        return '';
                    }
                }
            ],

            [
                'attribute' => 'Nakvynė nuo',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->bed_from)) {
                        return $model->bed_from;
                    } else {
                        return '';
                    }
                }
            ],

            [
                'attribute' => 'Nakvynė iki',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->bed_to)) {
                        return $model->bed_to;
                    } else {
                        return '';
                    }
                }
            ],

            [
                'attribute' => 'Atvykimo transportas',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->transport->name)) {
                        return $model->transport->name;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'Atvykimo miestas',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->capitals->name)) {
                        return $model->capitals->name;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'Atvykimo laikas',

                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->arrival)) {
                        return $model->arrival;
                    } else {
                        return '';
                    }

                },

            ],

            [
                'attribute' => 'Išvykimo transportas',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->offTransport->name)) {
                        return $model->offTransport->name;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'Išvykimo miestas',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->offCapital->name)) {
                        return $model->offCapital->name;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'Išvykimo laikas',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->departue)) {
                        return $model->departue;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'Atsakingo asmens telefonas',
                'value' => function ($model, $key, $index, $widget) use ($responsible_array) {
                    if (!empty($model->responsible_phone)) {
                        return $model->responsible_phone;
                    } else {
                        if(isset($responsible_array[$model->department_id]['responsible_phone'])) {
                            return $responsible_array[$model->department_id]['responsible_phone'];
                        }else{
                            return '';
                        }
                    }

                },
            ],

            [
                'attribute' => 'Atsakingo asmens el.paštas',
                'value' => function ($model, $key, $index, $widget) use ($responsible_array) {
                    if (!empty($model->responsible_mail)) {
                        return $model->responsible_mail;
                    } else {
                        if(isset($responsible_array[$model->department_id]['responsible_mail'])) {
                            return $responsible_array[$model->department_id]['responsible_mail'];
                        }else{
                            return '';
                        }
                    }


                },
            ],

            [
                'attribute' => 'Specialūs poreikiai',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->specials)) {
                        return $model->specials;
                    } else {
                        return '';
                    }

                },
            ],
        ],
    ]);

    ?>
</div>

