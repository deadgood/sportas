<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Pagal delegacijas';
$this->params['breadcrumbs'][] = 'Ataskaitos';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="tables-index">


    <?php

    $params = array();
    $county = '';
    foreach ($dataProviderDelegations->allModels as $value) {
        foreach ($value as $key => $val) {
            array_push($params, $key);
        }
    }

    $params = array_unique($params);




    echo GridView::widget([
        'dataProvider' => $dataProviderDelegations,
        'striped' => true,
        'hover' => true,
        'panel' => ['type' => 'primary', 'heading' => 'Pagal delegacijas'],
        'responsive' => true,

        'columns' => $params
    ]);


    ?>
</div>

