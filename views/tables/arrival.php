<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Atvykimas';
$this->params['breadcrumbs'][] = 'Ataskaitos';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="tables-index">


    <?php

    echo ExportMenu::widget([
        'dataProvider' => $dataProviderArrival,
        'columns' => [

            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->name)) {
                        return $model->name;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'surname',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->surname)) {
                        return $model->surname;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'age',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->age)) {
                        return $model->age;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'accreditation_first',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->accreditation_first)) {
                        return $model->accreditation_first;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'accreditation_second',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->accreditation_second)) {
                        return $model->accreditation_second;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'department_id',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->department->name)) {
                        return $model->department->name;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'county_id',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->county->country_name)) {
                        return $model->county->country_name;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'club',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->club)) {
                        return $model->club;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'responsible_phone',
                'value' => function ($model, $key, $index, $widget) use ($responsible_array) {
                    if (!empty($model->responsible_phone)) {
                        return $model->responsible_phone;
                    } else {
                        if(isset($responsible_array[$model->department_id]['responsible_phone'])) {
                            return $responsible_array[$model->department_id]['responsible_phone'];
                        }else{
                            return '';
                        }
                    }

                },
            ],

            [
                'attribute' => 'responsible_mail',
                'value' => function ($model, $key, $index, $widget) use ($responsible_array) {
                    if (!empty($model->responsible_mail)) {
                        return $model->responsible_mail;
                    } else {
                        if(isset($responsible_array[$model->department_id]['responsible_mail'])) {
                            return $responsible_array[$model->department_id]['responsible_mail'];
                        }else{
                            return '';
                        }
                    }


                },
            ],

            [
                'attribute' => 'arrival',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->transport->name)) {
                        return $model->transport->name;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'capitals_id',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->capitals->name)) {
                        return $model->capitals->name;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'arrival',

                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->arrival)) {
                        return $model->arrival;
                    } else {
                        return '';
                    }

                },

            ],

            [
                'attribute' => 'specials',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->specials)) {
                        return $model->specials;
                    } else {
                        return '';
                    }

                },
            ],
        ],
        'fontAwesome' => true,
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-default'
        ]
    ]);


    echo GridView::widget([
        'dataProvider' => $dataProviderArrival,
        'striped' => true,
        'hover' => true,
        'panel' => ['type' => 'primary', 'heading' => 'Bendra ataskaita'],
        'responsive' => true,
        'export' => false,
        'columns' => [

            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->name)) {
                        return $model->name;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'surname',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->surname)) {
                        return $model->surname;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'age',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->age)) {
                        return $model->age;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'accreditation_first',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->accreditation_first)) {
                        return $model->accreditation_first;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'accreditation_second',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->accreditation_second)) {
                        return $model->accreditation_second;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'department_id',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->department->name)) {
                        return $model->department->name;
                    } else {
                        return '';
                    }

                }

            ],

            [
                'attribute' => 'county_id',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->county->country_name)) {
                        return $model->county->country_name;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'club',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->club)) {
                        return $model->club;
                    } else {
                        return '';
                    }

                }
            ],

            [
                'attribute' => 'responsible_phone',
                'value' => function ($model, $key, $index, $widget) use ($responsible_array) {
                    if (!empty($model->responsible_phone)) {
                        return $model->responsible_phone;
                    } else {
                        if(isset($responsible_array[$model->department_id]['responsible_phone'])) {
                            return $responsible_array[$model->department_id]['responsible_phone'];
                        }else{
                            return '';
                        }
                    }

                },
            ],

            [
                'attribute' => 'responsible_mail',
                'value' => function ($model, $key, $index, $widget) use ($responsible_array) {
                    if (!empty($model->responsible_mail)) {
                        return $model->responsible_mail;
                    } else {
                        if(isset($responsible_array[$model->department_id]['responsible_mail'])) {
                            return $responsible_array[$model->department_id]['responsible_mail'];
                        }else{
                            return '';
                        }
                    }


                },
            ],

            [
                'attribute' => 'arrival',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->transport->name)) {
                        return $model->transport->name;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'capitals_id',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->capitals->name)) {
                        return $model->capitals->name;
                    } else {
                        return '';
                    }

                },
            ],

            [
                'attribute' => 'arrival',

                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->arrival)) {
                        return $model->arrival;
                    } else {
                        return '';
                    }

                },

            ],

            [
                'attribute' => 'specials',
                'value' => function ($model, $key, $index, $widget) {
                    if (!empty($model->specials)) {
                        return $model->specials;
                    } else {
                        return '';
                    }

                },
            ],
        ],
    ]);


    ?>
</div>

