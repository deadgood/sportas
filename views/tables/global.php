<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Pagal šalis';
$this->params['breadcrumbs'][] = 'Ataskaitos';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="tables-index">


    <?php

    $params = array();
    foreach ($dataProviderGlobal->allModels as $value) {
        foreach ($value as $key => $val) {
            array_push($params, $key);
        }
    }

    $params = array_unique($params);
    $params = array_reverse($params);


    echo GridView::widget([
        'dataProvider' => $dataProviderGlobal,
        'striped' => true,
        'hover' => true,
        'panel' => ['type' => 'primary', 'heading' => $this->title],
        'responsive' => true,

        'columns' => $params
    ]);?>
</div>



