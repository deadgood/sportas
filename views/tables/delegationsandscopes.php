<?php
use kartik\grid\GridView;

$this->title = 'Pagal delegacijas ir šakas';
$this->params['breadcrumbs'][] = 'Ataskaitos';
$this->params['breadcrumbs'][] = $this->title;

$params = array();
$county = '';
foreach ($dataProviderDelegationsAndScopes->allModels as $value) {
    foreach ($value as $key => $val) {
        array_push($params, $key);
    }
}

$params = array_unique($params);




echo GridView::widget([
    'dataProvider' => $dataProviderDelegationsAndScopes,
    'striped' => true,
    'hover' => true,
    'panel' => ['type' => 'primary', 'heading' => $this->title],
    'responsive' => true,

    'columns' => $params
]);