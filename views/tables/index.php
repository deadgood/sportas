<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Lentelės';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="tables-index">


<?php




//var_dump($dataProvider);












?>


    <?php

    echo GridView::widget([
        'dataProvider'=>$dataProviderOrganizations,



        'striped'=>true,
        'hover'=>true,
        'panel'=>['type'=>'primary', 'heading'=>'Delegacijos pagal organizacijas'],
        'responsive'=>true,
        'columns'=>[
            ['class'=>'kartik\grid\SerialColumn'],
            [

                'attribute'=>'Delegacija',
                'width'=>'310px',
                'value'=>function ($model, $key, $index, $widget) {
                    return $model['delegation'];
                },
                'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
            ],
            [

                'attribute'=>'Šalis',
                'width'=>'310px',
                'value'=>function ($model, $key, $index, $widget) {
                    return $model['country'];
                },
                'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
            ],
            [

                'attribute'=>'Nakvynė',
                'width'=>'310px',
                'value'=>function ($model, $key, $index, $widget) {
                    return $model['bed'];
                },
                'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
            ],
            [

                'attribute'=>'Transportas',
                'width'=>'310px',
                'value'=>function ($model, $key, $index, $widget) {
                    return $model['transport'];
                },
                'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
            ],
            [

                'attribute'=>'Atsakingas',
                'width'=>'310px',
                'value'=>function ($model, $key, $index, $widget) {
                    return $model['respname'];
                },
                'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
            ],
            [

                'attribute'=>'Paštas',
                'width'=>'310px',
                'value'=>function ($model, $key, $index, $widget) {
                    return $model['respmail'];
                },
                'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
            ],
            [

                'attribute'=>'Telefonas',
                'width'=>'310px',
                'value'=>function ($model, $key, $index, $widget) {
                    return $model['respphone'];
                },
                'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
            ],
        ],
    ]);
    ?>

<?php
    echo GridView::widget([
    'dataProvider'=>$dataProviderOrganizations,



    'striped'=>true,
    'hover'=>true,
    'panel'=>['type'=>'primary', 'heading'=>'Delegacijos pagal organizacijas'],
    'responsive'=>true,
    'columns'=>[
    ['class'=>'kartik\grid\SerialColumn'],
    [

    'attribute'=>'Delegacija',
    'width'=>'310px',
    'value'=>function ($model, $key, $index, $widget) {
    return $model['delegation'];
    },
    'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
    ],
    [

    'attribute'=>'Šalis',
    'width'=>'310px',
    'value'=>function ($model, $key, $index, $widget) {
    return $model['country'];
    },
    'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
    ],
    [

    'attribute'=>'Nakvynė',
    'width'=>'310px',
    'value'=>function ($model, $key, $index, $widget) {
    return $model['bed'];
    },
    'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
    ],
    [

    'attribute'=>'Transportas',
    'width'=>'310px',
    'value'=>function ($model, $key, $index, $widget) {
    return $model['transport'];
    },
    'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
    ],
    [

    'attribute'=>'Atsakingas',
    'width'=>'310px',
    'value'=>function ($model, $key, $index, $widget) {
    return $model['respname'];
    },
    'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
    ],
    [

    'attribute'=>'Paštas',
    'width'=>'310px',
    'value'=>function ($model, $key, $index, $widget) {
    return $model['respmail'];
    },
    'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
    ],
    [

    'attribute'=>'Telefonas',
    'width'=>'310px',
    'value'=>function ($model, $key, $index, $widget) {
    return $model['respphone'];
    },
    'contentOptions'=>['style'=>'text-align: center; vertical-align: middle;'],
    ],
    ],
    ]);
?>

</div>

