<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Departments */

$this->title = 'Sukurti delegaciją';
$this->params['breadcrumbs'][] = ['label' => 'Delegacijos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departments-create">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
