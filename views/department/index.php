<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Delegacijos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departments-index">

    

    <p>
        <?= Html::a('Sukurti delegaciją', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
