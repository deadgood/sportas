<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Departments */

$this->title = 'Atnaujinti delegaciją: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Delegacijos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="departments-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
