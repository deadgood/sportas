<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Transport */

$this->title = 'Sukurti tipą';
$this->params['breadcrumbs'][] = ['label' => 'Transporto tipas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transport-create">

  

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
