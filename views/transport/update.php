<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Transport */

$this->title = 'Atnaujinti transporto tipą: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Transporto tipas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="transport-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
