<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\acreditations */

$this->title = 'Sukurti akreditaciją';
$this->params['breadcrumbs'][] = ['label' => 'Akreditacijos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="acreditations-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
