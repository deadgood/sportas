<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Akreditacijos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="acreditations-index">

    <p>
        <?= Html::a('Sukurti akreditacija', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'dependency',

                'content' => function ($model) {
                    if ($model['dependency'] == 1) {
                        $text = 'Taip';
                    } else {
                        $text = 'Ne';
                    }
                    return $text;
                },
                'label' => 'Ar turi sporto šakų?',
                'enableSorting' => true
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        if ($model->id != 4) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                                ['acreditations/view', 'id' => $model->id], [
                                    'title' => 'View',
                                ]);
                        } else {
                            return '';
                        }
                    },
                    'update' => function ($url, $model) {
                        if ($model->id != 4) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                ['acreditations/edit', 'id' => $model->id], [
                                    'title' => 'Update',
                                ]);
                        } else {
                            return '';
                        }

                    },
                    'delete' => function ($url, $model) {
                        if ($model->id != 4) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                ['acreditations/delete', 'id' => $model->id], [
                                    'title' => 'Delete',
                                ]);
                        } else {
                            return '';
                        }
                    }

                ]
            ],
        ],
    ]); ?>
</div>
