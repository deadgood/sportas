<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\acreditations */

$this->title = 'Atnaujinti akreditaciją: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Akreditacijos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Atnaujinti';
?>
<div class="acreditations-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
