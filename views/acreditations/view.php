<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\acreditations */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Akreditacijos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="acreditations-view">

 

    <p>
        <?= Html::a('Atnaujinti', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Trinti', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'name',
            [
                'label'  => 'Ar turi sporto šakų?',
                'value'  => $model->dependency == 1 ? 'Taip' : 'Ne',

            ],
        ],


    ]) ?>

</div>
