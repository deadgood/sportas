<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
if(!empty(Yii::$app->user->identity)){



if (Yii::$app->user->identity->admin == 1) {
    $items =

        [
            ['label' => 'Akreditacijos', 'url' => ['/acreditations/index']],
            ['label' => 'Administratoriai', 'url' => ['/user/index']],
            ['label' => 'Delegacijų kodai', 'url' => ['/codes/index']],
            ['label' => 'Dalyviai', 'url' => ['/adminp/index']],
            ['label' => 'Delegacijos', 'url' => ['/department/index']],
            ['label' => 'Sporto šakos', 'url' => ['/scopes/index']],
            ['label' => 'Nakvynės datos', 'url' => ['/beds/index']],
            ['label' => 'Atvykimo datos', 'url' => ['/tour/index']],
            ['label' => 'Atvykimo miestas', 'url' => ['/capitals/index']],
            ['label' => 'Lentelės', 'url' => ['/tables/index']],
            Yii::$app->user->isGuest ? (
            ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li  id="log-out">'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    '<span class="glyphicon glyphicon-log-in" id="logout_span"></span>',
                    ['class' => 'btn btn-link','style'=>'color:black;font-weight: bold']
                )
                . Html::endForm()
                . '</li>'
            )
        ];
} else {
    $items =

        [

            Yii::$app->user->isGuest ? (
            ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Atsijungti (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link','style'=>'color:black']
                )
                . Html::endForm()
                . '</li>'
            )
        ];
}

}else{
    $items=[];
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([//        'brandLabel' => 'My Company',
        //        'brandUrl' => Yii::$app->homeUrl,
        'options' => ['class' => 'navbar-inverse navbar-fixed-top','style'=>'background-color:#ccff66;'],]);
    echo Nav::widget(['options' => ['class' => 'navbar-nav navbar-right','style'=>'background-color:#ccff66;color:black;'],
        'items' => $items]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
<!--        <p class="pull-left">&copy; My Company --><?//= date('Y') ?><!--</p>-->
<!---->
<!--        <p class="pull-right">--><?//= Yii::powered() ?><!--</p>-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
