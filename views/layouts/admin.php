<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

?>

<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<?php $this->beginPage() ?>

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>



    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Žaidiniū administravimas</title>
    <link rel="icon" type="image/ico" href="/style/assets/images/favicon.ico"/>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ============================================
    ================= Stylesheets ===================
    ============================================= -->
    <!-- vendor css files -->
    <link rel="stylesheet" href="/style/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="/style/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="/style/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="/style/assets/js/vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" href="/style/assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css">


    <link rel="stylesheet" href="/style/assets/css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!--    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')</script>-->

<!--    <script src="/style/assets/js/vendor/bootstrap/bootstrap.min.js"></script>-->

    <script src="/style/assets/js/vendor/jRespond/jRespond.min.js"></script>

    <script src="/style/assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>

    <script src="/style/assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>

    <script src="/style/assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>
    <script src="/style/assets/js/vendor/screenfull/screenfull.min.js"></script>

    <script src="/style/assets/js/vendor/daterangepicker/moment.min.js"></script>

    <script src="/style/assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <script src="/style/assets/js/vendor/slider/bootstrap-slider.min.js"></script>


    <script src="/style/assets/js/main.js"></script>
    <script src="/style/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="/js/admin.js"></script>

</head>


<body id="minovate" class="appWrapper">
<?php $this->beginBody() ?>

<div id="wrap" class="animsition">


    <section id="header">
        <header class="clearfix">

            <!-- Branding -->
            <div class="branding">
                <a class="brand" href="index.html">
                    <span><strong>PLSZ</strong>APP</span>
                </a>
                <a href="#" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav-right pull-right list-inline">


                <li class="dropdown nav-profile">

                    <a href="/admin/logout" class="dropdown-toggle">
                        <span><?= isset(Yii::$app->user->identity)?Yii::$app->user->identity->name : ''?><i class="fa fa-power-off" aria-hidden="true"></i></span>
                    </a>
                </li>


            </ul>


        </header>

    </section>

    <div id="controls">


        <aside id="sidebar">


            <div id="sidebar-wrap">

                <div class="panel-group slim-scroll" role="tablist">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#sidebarNav">
                                    Navigation <i class="fa fa-angle-up"></i>
                                </a>
                            </h4>
                        </div>
                        <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">


                                <!-- ===================================================
                                ================= NAVIGATION Content ===================
                                ==================================================== -->

                                <?php $this->beginContent('@app/views/layouts/menu.php');
                                $this->endContent(); ?>


                            </div>
                        </div>
                    </div>


                </div>

            </div>


        </aside>

        <aside id="rightbar">

            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#users" aria-controls="users" role="tab"
                                                              data-toggle="tab"><i class="fa fa-users"></i></a></li>
                    <li role="presentation"><a href="#history" aria-controls="history" role="tab" data-toggle="tab"><i
                                class="fa fa-clock-o"></i></a></li>
                    <li role="presentation"><a href="#friends" aria-controls="friends" role="tab" data-toggle="tab"><i
                                class="fa fa-heart"></i></a></li>
                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><i
                                class="fa fa-cog"></i></a></li>
                </ul>


                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="users">
                        <h6><strong>Online</strong> Users</h6>

                        <ul>

                            <li class="online">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle" src="/style/assets/images/ici-avatar.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Ing. Imrich <strong>Kamarel</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Ulaanbaatar, Mongolia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="online">
                                <div class="media">

                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/arnold-avatar.jpg" alt>
                                    </a>
                                    <span class="badge bg-lightred unread">3</span>

                                    <div class="media-body">
                                        <span class="media-heading">Arnold <strong>Karlsberg</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Bratislava, Slovakia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>

                                </div>
                            </li>

                            <li class="online">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle" src="/style/assets/images/peter-avatar.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Peter <strong>Kay</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Kosice, Slovakia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="online">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/george-avatar.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">George <strong>McCain</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Prague, Czech Republic</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="busy">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/random-avatar1.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Lucius <strong>Cashmere</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Wien, Austria</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="busy">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/random-avatar2.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Jesse <strong>Phoenix</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Berlin, Germany</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                        </ul>

                        <h6><strong>Offline</strong> Users</h6>

                        <ul>

                            <li class="offline">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/random-avatar4.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Dell <strong>MacApple</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Paris, France</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="offline">
                                <div class="media">

                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/random-avatar5.jpg" alt>
                                    </a>

                                    <div class="media-body">
                                        <span class="media-heading">Roger <strong>Flopple</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Rome, Italia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>

                                </div>
                            </li>

                            <li class="offline">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/random-avatar6.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Nico <strong>Vulture</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Kyjev, Ukraine</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="offline">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/random-avatar7.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Bobby <strong>Socks</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Moscow, Russia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="offline">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/random-avatar8.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Anna <strong>Opichia</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Budapest, Hungary</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="history">
                        <h6><strong>Chat</strong> History</h6>

                        <ul>

                            <li class="online">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle" src="/style/assets/images/ici-avatar.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Ing. Imrich <strong>Kamarel</strong></span>
                                        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor
                                        </small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="busy">
                                <div class="media">

                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/arnold-avatar.jpg" alt>
                                    </a>
                                    <span class="badge bg-lightred unread">3</span>

                                    <div class="media-body">
                                        <span class="media-heading">Arnold <strong>Karlsberg</strong></span>
                                        <small>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                            dolore eu fugiat nulla pariatur
                                        </small>
                                        <span class="badge badge-outline status"></span>
                                    </div>

                                </div>
                            </li>

                            <li class="offline">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle" src="/style/assets/images/peter-avatar.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Peter <strong>Kay</strong></span>
                                        <small>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                            deserunt mollit
                                        </small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="friends">
                        <h6><strong>Friends</strong> List</h6>
                        <ul>

                            <li class="online">
                                <div class="media">

                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/arnold-avatar.jpg" alt>
                                    </a>
                                    <span class="badge bg-lightred unread">3</span>

                                    <div class="media-body">
                                        <span class="media-heading">Arnold <strong>Karlsberg</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Bratislava, Slovakia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>

                                </div>
                            </li>

                            <li class="offline">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/random-avatar8.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Anna <strong>Opichia</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Budapest, Hungary</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="busy">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle"
                                             src="/style/assets/images/random-avatar1.jpg" alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Lucius <strong>Cashmere</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Wien, Austria</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                            <li class="online">
                                <div class="media">
                                    <a class="pull-left thumb thumb-sm" href="#">
                                        <img class="media-object img-circle" src="/style/assets/images/ici-avatar.jpg"
                                             alt>
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading">Ing. Imrich <strong>Kamarel</strong></span>
                                        <small><i class="fa fa-map-marker"></i> Ulaanbaatar, Mongolia</small>
                                        <span class="badge badge-outline status"></span>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="settings">
                        <h6><strong>Chat</strong> Settings</h6>

                        <ul class="settings">

                            <li>
                                <div class="form-group">
                                    <label class="col-xs-8 control-label">Show Offline Users</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch greensea">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="show-offline" checked="">
                                            <label class="onoffswitch-label" for="show-offline">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <label class="col-xs-8 control-label">Show Fullname</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch greensea">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="show-fullname">
                                            <label class="onoffswitch-label" for="show-fullname">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <label class="col-xs-8 control-label">History Enable</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch greensea">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="show-history" checked="">
                                            <label class="onoffswitch-label" for="show-history">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <label class="col-xs-8 control-label">Show Locations</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch greensea">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="show-location" checked="">
                                            <label class="onoffswitch-label" for="show-location">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <label class="col-xs-8 control-label">Notifications</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch greensea">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="show-notifications">
                                            <label class="onoffswitch-label" for="show-notifications">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <label class="col-xs-8 control-label">Show Undread Count</label>
                                    <div class="col-xs-4 control-label">
                                        <div class="onoffswitch greensea">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox"
                                                   id="show-unread" checked="">
                                            <label class="onoffswitch-label" for="show-unread">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>

        </aside>


    </div>

    <section id="content">

        <div class="page page-search-results">

            <div class="pageheader">

                <h2><b><?=$this->title ?> </b><span></span></h2>

                <div class="page-bar">

                    <ul class="page-breadcrumb">
                        <li>

                            <?= Html::a('<i class="fa fa-home"></i>Pradžia', ['/admin/index']) ?>
                        </li>

                            <?php


//var_dump(Yii::$app->controller->id);
                            foreach ($this->params['breadcrumbs'] as $breadcrumb) {
                             if(isset($breadcrumb['url'])){?>
                                <li>
                                 <?= Html::a($breadcrumb['label'], ['/'.Yii::$app->controller->id.'/'.$breadcrumb['url'][0]]) ?>
                        </li>
                         <?php    }else{ ?>
                              <li>
                                 <?= $breadcrumb ?>
                              </li>

                             <?php } ?>
<!--                                }-->
<!--                                ?>-->
<!---->
<!--                                <li>-->
<!--                                    <a href="index.html">--><?//= $breadcrumb?><!--</a>-->
<!--                                </li>-->
                           <?php } ?>

                        </li>
                    </ul>

                </div>

            </div>

            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-12">

                    <!-- tile -->
                    <section class="tile tile-simple">

                        <!-- tile body -->
                        <div class="tile-body p-0">


                            <div class="view_content" style="padding: 30px 15px 40px 15px;">

                            <?= $content ?>

                            </div>
                        </div>
                        <!-- /tile body -->

                    </section>
                    <!-- /tile -->
                </div>
                <!-- /col -->
            </div>
            <!-- /row -->

        </div>

    </section>
    <!--/ CONTENT -->


</div>
<footer class="footer">
    <div class="container">

    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
