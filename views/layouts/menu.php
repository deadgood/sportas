<?php
/**
 * Created by PhpStorm.
 * User: Matas
 * Date: 2016-07-25
 * Time: 17:13
 */
use yii\helpers\Html;

?>
<ul id="navigation">
    <li>
        <?= Html::a('<i class="fa fa-list"></i> <span>Delegacijos</span>', ['/department/index']) ?>
    </li>
    <li>
        <?= Html::a('<i class="fa fa-list"></i> <span>Akreditacijos</span>', ['/acreditations/index']) ?>
    </li>

    <li>
        <?= Html::a('<i class="fa fa-list"></i> <span>Sporto šakos</span>', ['/scopes/index']) ?>
    </li>

    <li>
        <?= Html::a('<i class="fa fa-list"></i> <span>Dalyviai</span>', ['/adminp/index']) ?>
    </li>


    <li>
        <?= Html::a('<i class="fa fa-list"></i> <span>Kodai</span>', ['/codes/index']) ?>
    </li>

    <li>
        <?= Html::a('<i class="fa fa-list"></i> <span>Nakvynės datos</span>', ['/beds/index']) ?>
    </li>
    <li>
        <?= Html::a('<i class="fa fa-list"></i> <span>Atvykimo miestas</span>', ['/capitals/index']) ?>
    </li>

    <li>
        <?= Html::a('<i class="fa fa-list"></i> <span>Transporto tipas</span>', ['transport/index']) ?>
    </li>

    <li>
        <?= Html::a('<i class="fa fa-list"></i> <span>Atvykimo datos</span>', ['/tour/index']) ?>
    </li>
    <li>
<!--        <i class="fa fa-caret-right"></i>-->
        <a role="button" tabindex="0"><i class="fa fa-list"></i> <span>Ataskaitos</span></a>
        <ul>
            <li><?= Html::a('<i class="fa fa-caret-right"></i>  Atvykimas', ['/tables/arrival'] ) ?></li>
            <li><?= Html::a('<i class="fa fa-caret-right"></i>  Išvykimas', ['/tables/departue'] ) ?></li>
            <li><?= Html::a('<i class="fa fa-caret-right"></i>  Pagal Šalis', ['/tables/global'] ) ?></li>
            <li><?= Html::a('<i class="fa fa-caret-right"></i>  Pagal delegacijas', ['/tables/ofdelegations'] ) ?></li>
            <li><?= Html::a('<i class="fa fa-caret-right"></i>  Pagal delegacijas ir šakas', ['/tables/delegationsandscopes'] ) ?></li>
            <li><?= Html::a('<i class="fa fa-caret-right"></i>  Dalyvių ataskaita', ['/tables/overall'] ) ?></li>
        </ul>
    </li>
    <li>
        <?= Html::a('<i class="fa fa-list"></i> <span>Administratoriai</span>', ['/user/index']) ?>
    </li>

    <li>
        <?= Html::a('<i class="fa fa-list"></i> <span>Šalys</span>', ['/countries/index']) ?>
    </li>

    <li>
        <?= Html::a('<i class="fa fa-list"></i> <span>Aktyvacija</span>', ['/settings/index']) ?>
    </li>


    <!--    <li>-->
    <!--        <a href="#"><i class="fa fa-list"></i> <span>Akreditacijos</span></a>-->
    <!--        <ul>-->
    <!--            <li><a href="form-common.html"><i class="fa fa-caret-right"></i> Common-->
    <!--                    Elements</a></li>-->
    <!--            <li><a href="form-validate.html"><i class="fa fa-caret-right"></i>-->
    <!--                    Validation Elements</a></li>-->
    <!--            <li><a href="form-wizard.html"><i class="fa fa-caret-right"></i> Form Wizard-->
    <!--                    <span class="badge badge-success">13</span></a></li>-->
    <!--            <li><a href="form-upload.html"><i class="fa fa-caret-right"></i> File Upload</a>-->
    <!--            </li>-->
    <!--            <li><a href="form-imgcrop.html"><i class="fa fa-caret-right"></i> Image Crop</a>-->
    <!--            </li>-->
    <!--        </ul>-->
    <!--    </li>-->


</ul>
