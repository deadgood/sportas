<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="basic_elements">
    <div class="participant-index">
        <!--    //Yii::$app->user->Identity->username-->
        <div class="row">
            <div class="col-lg-12" style="margin-bottom: 15px;">
                <h1>"<?php if (!empty(Yii::$app->user->Identity)) {
                        echo Yii::$app->user->Identity->username;
                    } ?>" delegacijos dalyvių sąrašas</h1>
            </div>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'summary' => "",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                'surname',


                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

        <?php if ($settings->value == 1) { ?>
            <div class="row">
                <div class="col-lg-2" style="float:right">
                    <?= Html::a('Sukurti dalyvį',
                        ['create'],
                        ['class' => 'btn btn-success', 'style' => 'float:right']) ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>