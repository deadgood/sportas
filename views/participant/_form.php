<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Participant;
use kartik\datetime\DateTimePicker;


$this->registerJsFile('/js/scripts.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
/* @var $this yii\web\View */
/* @var $model app\models\participant */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="participant-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="basic_elements">

        <?= $form->field($model, 'county_id')->dropdownlist($countyArr,
            ['prompt' => '-pasirinkite šalį:', 'id' => 'country_input', 'onchange' => 'departue(this)']) ?>

        <?= $form->field($model, 'club')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'sex_id')->dropdownlist($sex, ['prompt' => '-pasirinkite lytį:']) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'age')->dropdownlist($age, ['prompt' => '-pasirinkite amžių:']) ?>

    </div>


    <div class="basic_elements">
        <?= $form->field($model, 'accreditation_first')->dropdownlist($acreditationArr,
            [
                'prompt' => '-pasirinkite Akreditaciją:',
                'onchange' => 'scopesShow(this);',
                'id' => 'first_acr',
                'disabled' => $disabled
            ]) ?>

        <?php if ($settings->value == 1) { ?>
        <div class="row">
            <div class="col-lg-3" style="float:right;">
                <?= Html::a('+Pridėti akreditacija',
                    'javascript:void(0)',
                    [
                        'class' => 'btn btn-warning',
                        'onclick' => 'acreditations()',
                        'id' => 'acrplus',
                        'style' => 'float:right;',
                    ]) ?>
            </div>
        </div>
        <?php } ?>
        <div id="optional_acreditation" style="display: none">

            <?= $form->field($model, 'accreditation_second')->dropdownlist($acreditationArr,
                ['prompt' => '-pasirinkite Akreditaciją:', 'onchange' => 'scopesShow(this);', 'id' => 'second_acr','disabled'=>$disabled]) ?>
            <?php if ($settings->value == 1) { ?>
            <div class="row">
                <div class="col-lg-3" style="float:right;">
                    <?= Html::a('-Panaikinti akreditacija',
                        'javascript:void(0)',
                        [
                            'class' => 'btn btn-warning',
                            'onclick' => 'acreditations()',
                            'style' => 'float:right',
                        ]) ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>

    <div class="basic_elements" style="display:none" id="responsible_block">
        <?= $form->field($model, 'responsible_phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'responsible_mail')->textInput(['maxlength' => true]) ?>
    </div>

    <div id="scopes" style="display: none" class="basic_elements">
        <div class="scope_1">
            <?= $form->field($model, 'scope_first')->dropdownlist($scopes,
                ['prompt' => '-pasirinkite sporto šaką:', 'id' => 'scope_1' ,'disabled'=>$disabled]) ?>

            <?php if ($settings->value == 1) { ?>
            <div class="row">
                <div class="col-lg-3" style="float:right;">
                    <?= Html::a('+pridėti sporto šaką',
                        'javascript:void(0)',
                        [
                            'id' => 'first_scope',
                            'class' => 'btn btn-warning',
                            'onclick' => 'scopes(this)',
                            'style' => 'float:right;',
                        ]) ?>
                </div>
            </div
                <?php } ?>
        </div>
    </div>

    <div class="scope_2" style="display: none;">

        <?= $form->field($model, 'scope_second')->dropdownlist($scopes, ['prompt' => '-pasirinkite sporto šaką:','disabled'=>$disabled]) ?>
        <?php if ($settings->value == 1) { ?>
        <?= Html::a('+pridėti sporto šaką',
            'javascript:void(0)',
            [
                'id' => 'second_scope',
                'class' => 'btn btn-warning',
                'onclick' => 'scopes(this)',
                'style' => 'float:right',
            ]) ?>
        <?php } ?>
        <?php if ($settings->value == 1) { ?>
        <div class="row">
            <div class="col-lg-6" style="float:right;">
                <?= Html::a('-Panaikinti sporto šaką',
                    'javascript:void(0)',
                    [
                        'class' => 'btn btn-warning',
                        'onclick' => 'scopesdel(this)',
                        'style' => 'float:right',
                        'id' => 'first_delete',
                    ]) ?>
            </div>
        </div>

        <?php } ?>
    </div>

    <div class="scope_3" style="display:none">
        <?= $form->field($model, 'scope_third')->dropdownlist($scopes, ['prompt' => '-pasirinkite sporto šaką:','disabled'=>$disabled]) ?>
        <?php if ($settings->value == 1) { ?>
            <?= Html::a('+pridėti sporto šaką',
                'javascript:void(0)',
                [
                    'id' => 'third_scope',
                    'class' => 'btn btn-warning',
                    'onclick' => 'scopes(this)',
                    'style' => 'float:right'
                ]) ?>
        <?php } ?>


        <?php if ($settings->value == 1) { ?>

            <div class="row">
                <div class="col-lg-6" style="float:right;">
                    <?= Html::a('-Panaikinti sporto šaką',
                        'javascript:void(0)',
                        [
                            'class' => 'btn btn-warning',
                            'onclick' => 'scopesdel(this)',
                            'style' => 'float:right',
                            'id' => 'second_delete'
                        ]) ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<div class="basic_elements">

    <?= $form->field($model, 'bed')->checkbox(['onclick' => 'bedtime(this);']) ?>
    <div id="bed_block"></div>

</div>

<div class="basic_elements">

    <input name="arrival_custom" type="checkbox" id="arrival_custom" onclick="showArrivals()">
    <label>Atvyksiu savarankiškai" (nereikia pasitikti)</label>
</div>

<div class="basic_elements">

    <input name="departure_custom" type="checkbox" id="departure_custom" onclick="showDeparture()">
    <label>Išvyksiu savarankiškai" (nereikia išlydėti)</label>
</div>


<div class="basic_elements" id="departue_block" style="display: none;">

    <div class="arrival-block">

        <?= $form->field($model, 'transport_id')->dropdownlist($transport,
            ['prompt' => '-pasirinkite atvykimo vietą:']) ?>

        <?= $form->field($model, 'capitals_id')->dropdownlist($capitals,
            ['prompt' => '-pasirinkite atvykimo miestą:']) ?>

        <?php if ($model->isNewRecord) {
        } else {
            echo Html::hiddenInput('get', $_GET['id'], ['id' => 'item_id']);
        }

        ?>

        <div style="margin-bottom: 10px">
            <?php
            echo '<label class="control-label">Atvykimo data/laikas</label>';
            echo DateTimePicker::widget([
                'model' => $model,
                'id' => 'arrival',
                'attribute' => 'arrival',
                'options' => ['placeholder' => '-pasirinkite atvykimo data/laiką:'],

                'pluginOptions' => [
                    'autoclose' => true,
                    'startDate' => $arrivalInterval['arrival_from'],
                    'endDate' => $arrivalInterval['arrival_to'],
                    'format' => 'yyyy-mm-dd HH:ii',
                ]
            ]);
            ?>
        </div>

    </div>


    <div class="departure_block">

        <?= $form->field($model, 'off_transport_id')->dropdownlist($transport,
            ['prompt' => '-pasirinkite išvykimo miestą:']) ?>

        <?= $form->field($model, 'off_capital_id')->dropdownlist($capitals,
            ['prompt' => '-pasirinkite išvykimo vietą:']) ?>

        <div style="margin-bottom: 10px">
            <?php
            echo '<label class="control-label">Išvykimo data/laikas</label>';
            echo DateTimePicker::widget([
                'model' => $model,
                'attribute' => 'departue',
                'options' => ['placeholder' => '-pasirinkite išvykimo data/laiką:'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd HH:ii',
                    'autoclose' => true,
                    'startDate' => $arrivalInterval['arrival_from'],
                    'endDate' => $arrivalInterval['arrival_to'],

                ]
            ]);
            ?>
        </div>
    </div>
</div>

<div class="basic_elements">

    <?= $form->field($model, 'specials')->textarea() ?>

    <?= $form->field($model, 'agreed')->checkbox() ?>

</div>


<div class="row" style="text-align: center">
    <?= Html::submitButton($model->isNewRecord ? 'Sukurti' : 'Atnaujinti',
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => 'margin-top:20px;']) ?>
    <?= Html::a('Grįžti atgal',
        ['/participant/index'],
        ['class' => 'btn btn-warning', 'style' => 'margin-top:20px;']) ?>

</div>
<?php ActiveForm::end(); ?>

</div>
