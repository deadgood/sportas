<?php
/**
 * Created by PhpStorm.
 * User: Matas
 * Date: 2016-07-01
 * Time: 13:34
 */
use kartik\date\DatePicker;
?>
<div id="bed_block">
    <div style="margin-bottom: 10px">
        <?php
        echo '<label class="control-label">Nakvynė nuo</label>';
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'bed_from',
            'options' => ['placeholder' => 'data iki',],
            'pluginOptions' => [
                'autoclose'=>true,
                'startDate'=>$dateLapse['bed_from'],
                'endDate'=>$dateLapse['bed_to'],
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        ?>
    </div>
    <div style="margin-bottom: 10px">
        <?php
        echo '<label class="control-label">Nakvynė iki (išvykimo diena)</label>';
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'bed_to',
            'options' => ['placeholder' => 'data iki'],
            'id'=>'start',
            'pluginOptions' => [
                'autoclose'=>true,
                'startDate'=>$dateLapse['bed_from'],
                'endDate'=>$dateLapse['bed_to'],
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        ?>
    </div>

</div>
