<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\participant */

$this->title = 'Dalyvio sukūrimas';

?>
<div class="participant-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'countyArr'=>$countyArr,
        'sex'=>$sex,
        'age'=>$age,
        'acreditationArr'=>$acreditationArr,
        'scopes'=>$scopes,
        'transport' => $transport,
        'capitals'=>$capitals,
         'arrivalInterval'=>$arrivalInterval

    ]) ?>

</div>
