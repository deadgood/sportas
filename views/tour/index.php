<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Atvykimo datos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-index">


    <p>
        <?= Html::a('Sukurti atvykimo datą', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          
            'name',
            'arrival_from',
            'arrival_to',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
