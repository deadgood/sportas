<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\tour */

$this->title = 'Atnaujinti atvykimo datą: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Atvykimo datos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Atnaujinti';
?>
<div class="tour-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
