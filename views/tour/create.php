<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\tour */

$this->title = 'Sukurti atvykimo datą';
$this->params['breadcrumbs'][] = ['label' => 'Atvykimo datos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-create">

  

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
