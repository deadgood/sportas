<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
/* @var $this yii\web\View */
/* @var $model app\models\tour */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tour-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


    <div style="margin-bottom: 10px">
        <?php
        echo '<label class="control-label">Atvykimas nuo</label>';
        echo DateTimePicker::widget([
            'model' => $model,
            'attribute' => 'arrival_from',
            'options' => ['placeholder' => 'data nuo',],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd HH:ii::ss',
            ]
        ]);
        ?>
    </div>


    <div style="margin-bottom: 10px">
        <?php
        echo '<label class="control-label">Atvykimas iki</label>';
        echo DateTimePicker::widget([
            'model' => $model,
            'attribute' => 'arrival_to',
            'options' => ['placeholder' => 'data iki',],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd HH:ii:ss',
            ]
        ]);
        ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Sukurti' : 'Atnaujinti', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
