<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Aktyvacija';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <h1>Registracijos būsena</h1>

    <?php $form = ActiveForm::begin([
        'id' => 'activation'
    ]); ?>

    <div class="form-group activation-button">

        <?php if ($model->value == 0) {
            echo Html::submitButton('Aktyvuoti',
                ['class' => 'btn btn-success', 'value' => $model->value, 'style' => 'width:100%']);
            $model->value = 1;
            echo $form->field($model, 'value')->hiddenInput()->label(false);

        } else {
            echo Html::submitButton('Išjungti',
                ['class' => 'btn btn-danger', 'value' => $model->value, 'style' => 'width:100%']);
            $model->value = 0;
            echo $form->field($model, 'value')->hiddenInput()->label(false);
        } ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
