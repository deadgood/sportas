<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\capitals */

$this->title = 'Atnaujinti atvykimo miestą: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Atvykimo miestas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Atnaujinti';
?>
<div class="capitals-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
