<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\capitals */

$this->title = 'Skurti atvykimo miestą';
$this->params['breadcrumbs'][] = ['label' => 'Atvykimo miestas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="capitals-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
