<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="site-login">
    <div class="row" style="text-align: center">
        <img src="/uploads/lsfs.png">
    </div>
    <div class="row"
         style="text-align: center;border:1px solid #d9d9d9;margin-top: 5%;margin-left: 10%;margin-right: 10%;box-shadow: 0 15px 20px #888888;">
        <div class="col-lg-7" style="margin: 0 auto;float:none;margin-top: 20px;">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form-horizontal'],

            ]); ?>
            <div style="padding-left: 10px;padding-right: 10px">
                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            </div>
            <div style="padding-left: 10px;padding-right: 10px">
                <?= $form->field($model, 'password')->passwordInput() ?>
            </div>
            <?= $form->field($model, 'rememberMe')->checkbox([

            ]) ?>


            <?= Html::submitButton('Prisijungti', ['class' => 'btn btn-success', 'name' => 'login-button', 'style' => 'margin-bottom:20px;']) ?>



            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
