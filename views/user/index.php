<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Administratoriai';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

  

    <p>
        <?= Html::a('sukurti administratoriu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
            'username',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
