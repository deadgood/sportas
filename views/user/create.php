<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\user */

$this->title = 'Sukurti Administratorių';
$this->params['breadcrumbs'][] = ['label' => 'Administratoriai', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

  

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
