<?php


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\beds */

$this->title = 'Kurti nakvynę';
$this->params['breadcrumbs'][] = ['label' => 'Nakvynės datos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beds-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
