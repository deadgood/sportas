<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\beds */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="beds-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div style="margin-bottom: 10px">
        <?php
        echo '<label class="control-label">Nakvynė nuo</label>';
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'bed_from',
            'options' => ['placeholder' => 'data nuo',],
            'pluginOptions' => [
                'autoclose' => true,

            ]
        ]);
        ?>
    </div>
    <div style="margin-bottom: 10px">
        <?php
        echo '<label class="control-label">Nakvynė iki (išvykimo diena)</label>';
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'bed_to',
            'options' => ['placeholder' => 'data iki',],
            'pluginOptions' => [
                'autoclose' => true,

            ]
        ]);
        ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Sukurti' : 'Atnaujinti', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
