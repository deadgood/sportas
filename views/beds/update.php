<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\beds */

$this->title = 'Atnaujinti nakvynę: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Nakvynės datos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Atnaujinti';
?>
<div class="beds-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
