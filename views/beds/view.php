<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\beds */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Nakvynės datos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beds-view">



    <p>
        <?= Html::a('Atnaujinti', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Trinti', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'bed_from',
            'bed_to',
          
            
        ],
    ]) ?>

</div>
