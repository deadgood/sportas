<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nakvynės datos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beds-index">

 

    <p>
        <?= Html::a('Kurti nakvynę', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'bed_from',
            'bed_to',



            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
