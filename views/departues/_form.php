<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\departues */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="departues-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'department_id')->textInput() ?>

    <?= $form->field($model, 'transport_id')->textInput() ?>

    <?= $form->field($model, 'capitals_id')->textInput() ?>

    <?= $form->field($model, 'arrival')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'departue')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Sukurti' : 'Atnaujinti', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
