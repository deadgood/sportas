<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Išvykimo miestas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departues-index">

    

    <p>
        <?= Html::a('Sukurti išvykimo miestą', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'department_id',
            'transport_id',
            'capitals_id',
            'arrival',
            // 'departue',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
