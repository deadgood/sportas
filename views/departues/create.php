<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\departues */

$this->title = 'Sukurti išvykimo miestas';
$this->params['breadcrumbs'][] = ['label' => 'Departues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departues-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
