<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="participant-index">
    <!--    //Yii::$app->user->Identity->username-->
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Sukurti dalyvį', ['create','department'=>$_POST['id']], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => "",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'surname',


            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'update' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>',
                            ['/adminp/update','id'=>$model->id,'department'=>$_POST['id']],
                            [
                                'title' => 'Download',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                    'view' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>',
                            ['/adminp/view','id'=>$model->id,'department'=>$_POST['id']],
                            [
                                'title' => 'Download',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                    'delete' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            ['/adminp/delete','id'=>$model->id,'department'=>$_POST['id']],
                            [
                                'title' => 'Download',
                                'data-pjax' => '0',
                            ]
                        );
                    },

                ],
            ],
        ],
    ]); ?>
</div>
