<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\participant */

$this->title = 'Atnaujinti dalyvį: ' . $model->name;

?>
<div class="participant-update">

  

    <?= $this->render('_form', [
        'model' => $model,
        'countyArr' => $countyArr,
        'sex' => $sex,
        'age' => $age,
        'acreditationArr' => $acreditationArr,
        'scopes' => $scopes,
        'transport' => $transport,
        'capitals'=>$capitals,
        'arrivalInterval'=>$arrivalInterval
    ]) ?>

</div>

