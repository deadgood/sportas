<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\participant */

$this->title = $model->name . ' ' . $model->surname;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="basic_elements">
    <div class="participant-view">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Grįžti atgal', ['/adminp/index', 'department' => $_GET['department']],
                ['class' => 'btn btn-warning']) ?>

            <?= Html::a('Atnaujinti', ['update', 'id' => $model->id, 'department' => $_GET['department']],
                ['class' => 'btn btn-primary']) ?>

            <?= Html::a('Trinti', ['delete', 'id' => $model->id, 'department' => $_GET['department']], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>

        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'label' => $model->getAttributeLabel('department_id'),
                    'value' => !empty($model->department_id) ? $model->department->name : '-',

                ],

                [
                    'label' => $model->getAttributeLabel('county_id'),
                    'value' => !empty($model->county_id) ? $model->county->country_name : '-',

                ],

                'club',

                [
                    'label' => $model->getAttributeLabel('sex_id'),
                    'value' => !empty($model->sex) ? $model->sex->name : '-',

                ],

                'name',

                'surname',

                'age',

                [
                    'label' => $model->getAttributeLabel('accreditation_first'),
                    'value' => $firstAcr

                ],

                [
                    'label' => $model->getAttributeLabel('accreditation_second'),
                    'value' => !empty($secondAcr) ? $secondAcr : '-'

                ],

                [
                    'label' => $model->getAttributeLabel('responsible_phone'),
                    'value' => !empty($model->responsible_phone) ? $model->responsible_phone : '-',

                ],
                [
                    'label' => $model->getAttributeLabel('responsible_mail'),
                    'value' => !empty($model->responsible_mail) ? $model->responsible_mail : '-',

                ],

                [
                    'label' => $model->getAttributeLabel('scope_first'),
                    'value' => !empty($model->scope_first) ? $model->scopeFirst->name : '-',

                ],
                [
                    'label' => $model->getAttributeLabel('scope_second'),
                    'value' => !empty($model->scope_second) ? $model->scopeSecond->name : '-',

                ],

                [
                    'label' => $model->getAttributeLabel('scope_third'),
                    'value' => !empty($model->scope_third) ? $model->scopeThird->name : '-',

                ],

                [
                    'label' => $model->getAttributeLabel('bed_from'),
                    'value' => !empty($model->bed_from) ? $model->bed_from : '-',

                ],
                [
                    'label' => $model->getAttributeLabel('bed_to'),
                    'value' => !empty($model->bed_to) ? $model->bed_to : '-',

                ],

                [
                    'label' => $model->getAttributeLabel('transport_id'),
                    'value' => !empty($model->transport_id) ? $model->transport->name : '-',
                ],

                [
                    'label' => $model->getAttributeLabel('capitals_id'),
                    'value' => !empty($model->capitals_id) ? $model->capitals->name : '-',
                ],

                [
                    'label' => $model->getAttributeLabel('arrival'),
                    'value' => !empty($model->arrival) ? $model->arrival : '-'

                ],

                [
                    'label' => $model->getAttributeLabel('off_transport_id'),
                    'value' => !empty($model->off_transport_id) ? $model->offTransport->name : '-',

                ],

                [
                    'label' => $model->getAttributeLabel('off_capital_id'),
                    'value' => !empty($model->off_capital_id) ? $model->offCapital->name : '-',

                ],

                [
                    'label' => $model->getAttributeLabel('departue'),
                    'value' => !empty($model->departue) ? $model->departue : '-'

                ],

                'specials',

                [
                    'label' => $model->getAttributeLabel('agreed'),
                    'value' => !empty($model->agreed) ? 'Taip' : '-',

                ],

            ],
        ]) ?>

    </div>
</div>