<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


?>
<div class="site-login" style="text-align: center;margin-top: 2%;">
    <div class="row" style="text-align: center">
        <img src="/uploads/lsfs.png">
    </div>
    <div class="row"
         style="text-align: center;border:1px solid #d9d9d9;margin-top: 5%;margin-left: 10%;margin-right: 10%;box-shadow: 0 15px 20px #888888;">
        <div class="col-lg-7" style="margin: 0 auto;float:none;">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form-horizontal'],

            ]); ?>
            <label style="font-size: 20px;padding-bottom: 20px;padding-top: 20px;">Prieiga prie LSFS informacinės
                registracijos sistemos </label>

            <div style="padding-left: 10px;padding-right: 10px">
                <?= $form->field($model, 'code')->textInput(['autofocus' => true, 'style' => 'height:50px;padding-left:10px;padding-right:10px;', 'id' => 'inputGroup', 'placeholder' => 'Įveskite delegacijos kodą'])->label(false) ?>
            </div>

            <?= Html::submitButton('Prisijungti', ['class' => 'btn btn-success', 'name' => 'login-button', 'style' => 'margin-bottom:20px;']) ?>


            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
