<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div style="display: flex;justify-content: center;margin-top: 10%;">
<div class="site-error" style="width: 60%">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger" style="">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        The above error occurred while the Web server was processing your request.
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p>

</div>
</div>