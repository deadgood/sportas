<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\scopes */

$this->title = 'Sukurti sporto šaką';
$this->params['breadcrumbs'][] = ['label' => 'Sporto šakos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scopes-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
