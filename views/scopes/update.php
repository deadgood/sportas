<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\scopes */

$this->title = 'Atnaujinti sporto šaką: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sporto šakos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="scopes-update">

  

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
