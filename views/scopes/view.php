<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\scopes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sporto šakos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scopes-view">



    <p>
        <?= Html::a('Atnaujinti', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Trinti', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           
            'name',
        ],
    ]) ?>

</div>
