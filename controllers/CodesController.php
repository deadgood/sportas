<?php

namespace app\controllers;

use app\models\Departments;
use Yii;
use app\models\User;
use app\models\Codes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;

/**
 * CodesController implements the CRUD actions for codes model.
 */
class CodesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],

            'as access' => [
                'class' => '\app\components\AccessBehavior',
            ]
        ];
    }

    /**
     * Lists all codes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'admin';
        $dataProvider = new ActiveDataProvider([
            'query' => codes::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single codes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->layout = 'admin';
        $model = Codes::find()->where(['id' => $id])->with('department')->one();
      
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new codes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $this->layout = 'admin';
        $model = new codes();

        $departments = ArrayHelper::map(Departments::find()->all(), 'id', 'name');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {


            $modelUser = new User;
            $depName = Departments::find()->where(['id' => $_POST['Codes']['department_id']])->one();
            $modelUser->username = $depName['name'];
            $modelUser->password = md5($_POST['Codes']['code']);
            $modelUser->active = 0;
            $modelUser->admin = 0;
            $modelUser->department_id = $_POST['Codes']['department_id'];
            $modelUser->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            $code = Yii::$app->functions->randomString(30);
            $model->code = $code;
            return $this->render('create', [
                'model' => $model, 'departments' => $departments
            ]);
        }
    }

    /**
     * Updates an existing codes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $this->layout = 'admin';
        $model = $this->findModel($id);
        $firstmodel = $model;
        $departments = ArrayHelper::map(Departments::find()->all(), 'id', 'name');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelUser = User::find()->where(['password' => md5($firstmodel['code'])])->one();
            $depName = Departments::find()->where(['id' => $_POST['Codes']['department_id']])->one();
            $modelUser->username = $depName['name'];
            $modelUser->password = md5($_POST['Codes']['code']);
            $modelUser->department_id = $_POST['Codes']['department_id'];
            $modelUser->active = 0;
            $modelUser->admin = 0;
            $modelUser->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model, 'departments' => $departments
            ]);
        }
    }

    /**
     * Deletes an existing codes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->layout = 'admin';
        $model = $this->findModel($id);
        $userModel = User::find()->where(['password' => md5($model['code'])])->one();
        $userModel->delete();
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the codes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return codes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = codes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
