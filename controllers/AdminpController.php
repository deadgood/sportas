<?php

namespace app\controllers;

use app\models\Acreditations;
use app\models\Beds;
use app\models\Capitals;
use app\models\Countries;
use app\models\Scopes;
use app\models\Sex;
use app\models\Tour;
use app\models\Transport;
use Yii;
use app\models\Participant;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use app\models\Departments;

/**
 * ParticipantController implements the CRUD actions for participant model.
 */
class AdminpController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],

            'as access' => [
                'class' => '\app\components\AccessBehavior',
            ]
        ];
    }

    /**
     * Lists all participant models.
     * @return mixed
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionDates()
    {
        if (isset($_POST['county'])) {

            $this->layout = 'white';
            if (isset($_POST['iditem'])) {
                $model = Participant::find()->where(['id' => $_POST['iditem']])->one();
            } else {
                $model = new Participant();
            }
            if ($_POST['county'] == 127) {
                $dateLapse = Beds::find()->where(['id' => 4])->one();
            } else {
                $dateLapse = Beds::find()->where(['id' => 5])->one();
            }

            return $this->render('/adminp/dates', ['dateLapse' => $dateLapse, 'model' => $model]);
        }
    }

    public function actionIndex()
    {

        $this->layout = 'admin';
        $departments = ArrayHelper::map(Departments::find()->all(), 'id', 'name');
        return $this->render('index', [
            'departments' => $departments,
        ]);
    }

    public function actionMain()
    {
//        $this->layout='admin';
        if (isset($_POST)) {

            $this->layout = 'white';
            $dataProvider = new ActiveDataProvider([
                'query' => participant::find()->where(['department_id' => $_POST['id']]),
            ]);
            $dataProvider->pagination  = false;
            return $this->render('/adminp/main', [
                'dataProvider' => $dataProvider,
            ]);
        }


    }


    /**
     * Displays a single participant model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->layout = 'admin';
        $model = Participant::find()->where(['id' => $id])->
        with('county')->
        with('department')->
        with('sex')->
        with('scopeFirst')->
        with('scopeSecond')->
        with('scopeThird')->
        with('capitals')->
        with('transport')->
        with('offCapital')->
        with('offTransport')->
        one();

        $explodedfirst = explode('_', $model->accreditation_first);
        $explodedfirst = $explodedfirst[0];
        $firstAcr = Acreditations::find()->where(['id' => $explodedfirst])->one();
        $firstAcr = $firstAcr['name'];
        if (!empty($model->accreditation_second)) {
            $explodedsecond = explode('_', $model->accreditation_second);
            $explodedsecond = $explodedsecond[0];
            $model->accreditation_second = $explodedsecond;
            $secondAcr = Acreditations::find()->where(['id' => $explodedsecond])->one();
            $secondAcr = $secondAcr['name'];
        } else {
            $secondAcr = array();
        }


        $model->accreditation_first = $explodedfirst;
        $model->arrival = substr($model->arrival, 0, -3);
        $model->departue = substr($model->departue, 0, -3);
        return $this->render('view', [
            'model' => $model,
            'secondAcr' => $secondAcr,
            'firstAcr' => $firstAcr
        ]);
    }

    /**
     * Creates a new participant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'admin';
        $countyArr = ArrayHelper::map(Countries::find()->orderBy(['country_name'=>SORT_ASC])->all(), 'id', 'country_name');
        $sex = ArrayHelper::map(Sex::find()->all(), 'id', 'name');
        $transport = ArrayHelper::map(Transport::find()->all(), 'id', 'name');

        $age = range(12, 100);
        $tmpAge = array();
        foreach ($age as $key => $item) {
            $tmpAge[$item] = $item;
        }
        $age = $tmpAge;

        if (!empty(Yii::$app->user->Identity)) {
            $respCheck = Participant::find()->where(['department_id' => Yii::$app->user->Identity->department_id])->andWhere(['responsible' => 1])->one();
        }

        if (empty($respCheck)) {
            $acreditationArr = Acreditations::find()->all();
        } else {
            $acreditationArr = Acreditations::find()->where(['<>', 'name', 'Delegacijos vadovas'])->all();
        }

        $capitals = ArrayHelper::map(Capitals::find()->all(), 'id', 'name');
        $arrivalInterval = Tour::find()->where(['id' => 2])->one();
        foreach ($acreditationArr as $key => $item) {
            $acreditation[$item['id'] . '_' . $item['dependency']] = $item['name'];
        }
        $acreditationArr = $acreditation;
        $scopes = ArrayHelper::map(Scopes::find()->all(), 'id', 'name');
        $model = new participant();


        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());
            $model->department_id = !empty($_GET['department']) ? $_GET['department'] : '';

            if ($model->accreditation_first == '4_0' || $model->accreditation_second == '4_0') {
                $model->responsible = 1;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id, 'department' => $_GET['department']]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'countyArr' => $countyArr,
                'sex' => $sex,
                'age' => $age,
                'acreditationArr' => $acreditationArr,
                'scopes' => $scopes,
                'transport' => $transport,
                'capitals' => $capitals,
                'arrivalInterval' => $arrivalInterval
            ]);
        }
    }

    /**
     * Updates an existing participant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = 'admin';
        $model = $this->findModel($id);
        $countyArr = ArrayHelper::map(Countries::find()->orderBy(['country_name'=>SORT_ASC])->all(), 'id', 'country_name');
        $sex = ArrayHelper::map(Sex::find()->all(), 'id', 'name');
        $transport = ArrayHelper::map(Transport::find()->all(), 'id', 'name');

        $age = range(12, 100);
        $tmpAge = array();
        foreach ($age as $key => $item) {
            $tmpAge[$item] = $item;
        }
        $age = $tmpAge;

        if (!empty(Yii::$app->user->Identity)) {
            $respCheck = Participant::find()->where(['department_id' => Yii::$app->user->Identity->department_id])->andWhere(['responsible' => 1])->one();
        }

        if ($model->responsible != 1) {
            if (empty($respCheck)) {
                $acreditationArr = Acreditations::find()->all();
            } else {
                $acreditationArr = Acreditations::find()->where(['<>', 'name', 'Delegacijos vadovas'])->all();
            }
        } else {
            $acreditationArr = Acreditations::find()->all();
        }

        $acreditationArr = Acreditations::find()->all();
        $capitals = ArrayHelper::map(Capitals::find()->all(), 'id', 'name');
        $arrivalInterval = Tour::find()->where(['id' => 2])->one();
        foreach ($acreditationArr as $key => $item) {
            $acreditation[$item['id'] . '_' . $item['dependency']] = $item['name'];

            if($model->accreditation_first == $item['id']){
                $model->accreditation_first = $item['id'] . '_' . $item['dependency'];
            }
            if($model->accreditation_second == $item['id']){
                $model->accreditation_second = $item['id'] . '_' . $item['dependency'];
            }
        }
        $acreditationArr = $acreditation;
        $scopes = ArrayHelper::map(Scopes::find()->all(), 'id', 'name');

        if (Yii::$app->request->post()) {

            if ($model->accreditation_first == '4_0' || $model->accreditation_second == '4_0') {
                $model->responsible = 1;
            }

            if (!isset($_POST['arrival_custom'])) {
                $model->transport_id = null;
                $model->capitals_id = null;
                $model->arrival = null;
            }

            if (isset($_POST['departure_custom'])) {
                $model->off_transport_id = null;
                $model->off_capital_id = null;
                $model->departue = null;
            }

            if ($_POST['Participant']['bed']) {
                $model->bed_from = null;
                $model->bed_to = null;
            }

            $model->load(Yii::$app->request->post());
            $model->save();

            return $this->redirect(['view', 'id' => $model->id, 'department' => $_GET['department']]);

        } else {
            $model->arrival = substr($model->arrival, 0, -3);
            $model->departue = substr($model->departue, 0, -3);
            return $this->render('update', [
                'model' => $model,
                'countyArr' => $countyArr,
                'sex' => $sex,
                'age' => $age,
                'acreditationArr' => $acreditationArr,
                'scopes' => $scopes,
                'transport' => $transport,
                'capitals' => $capitals,
                'arrivalInterval' => $arrivalInterval
            ]);
        }
    }

    /**
     * Deletes an existing participant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->layout = 'admin';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the participant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return participant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = participant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
