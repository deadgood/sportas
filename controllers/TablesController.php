<?php

namespace app\controllers;

use app\models\Acreditations;
use app\models\Countries;
use app\models\Departments;
use app\models\Participant;
use app\models\Scopes;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

class TablesController extends \yii\web\Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],

            'as access' => [
                'class' => '\app\components\AccessBehavior',
            ]
        ];
    }

    public function actionArrival()
    {
        $this->layout = 'admin';

        $acreditations = ArrayHelper::map(Acreditations::find()->all(), 'id', 'name');
        $scopes = ArrayHelper::map(Scopes::find()->all(), 'id', 'name');

        $participants = Participant::find()->where([
            '<>',
            'county_id',
            127
        ])->with('county')->with('department')->with('sex')->with('transport')->with('offTransport')->with('capitals')->with('offCapital')->all();

        $responsible_array = array();
        foreach ($participants as $participant) {
            if (!empty($participant->responsible_phone)) {
                $responsible_array[$participant->department_id]['responsible_phone'] = $participant->responsible_phone;
            }

            if (!empty($participant->responsible_mail)) {
                $responsible_array[$participant->department_id]['responsible_mail'] = $participant->responsible_mail;
            }
        }

        foreach ($participants as $key => $participant) {

            if (!empty($participant->accreditation_first)) {
                $participants[$key]->accreditation_first = $acreditations[$participant->accreditation_first];
            }

            if (!empty($participant->accreditation_second)) {
                $participants[$key]->accreditation_second = $acreditations[$participant->accreditation_second];
            }

            if (!empty($participant->scope_first)) {
                $participants[$key]->scope_first = $scopes[$participant->scope_first];
            }

            if (!empty($participant->scope_second)) {
                $participants[$key]->scope_second = $scopes[$participant->scope_second];
            }

            if (!empty($participant->scope_third)) {
                $participants[$key]->scope_third = $scopes[$participant->scope_third];
            }

        }

        $dataProviderOverall = new ArrayDataProvider([
            'allModels' => $participants,
        ]);
        $dataProviderOverall->pagination = false;


        return $this->render('arrival',
            [
                'dataProviderArrival' => $dataProviderOverall,
                'responsible_array' => $responsible_array
            ]);
    }

    public function actionDepartue()
    {
        $this->layout = 'admin';
        $acreditations = ArrayHelper::map(Acreditations::find()->all(), 'id', 'name');
        $scopes = ArrayHelper::map(Scopes::find()->all(), 'id', 'name');
        $participants = Participant::find()->where([
            '<>',
            'county_id',
            127
        ])->with('county')->with('department')->with('sex')->with('transport')->with('offTransport')->with('capitals')->with('offCapital')->all();

        $responsible_array = array();
        foreach ($participants as $participant) {
            if (!empty($participant->responsible_phone)) {
                $responsible_array[$participant->department_id]['responsible_phone'] = $participant->responsible_phone;
            }

            if (!empty($participant->responsible_mail)) {
                $responsible_array[$participant->department_id]['responsible_mail'] = $participant->responsible_mail;
            }
        }

        foreach ($participants as $key => $participant) {

            if (!empty($participant->accreditation_first)) {
                $participants[$key]->accreditation_first = $acreditations[$participant->accreditation_first];
            }

            if (!empty($participant->accreditation_second)) {
                $participants[$key]->accreditation_second = $acreditations[$participant->accreditation_second];
            }

            if (!empty($participant->scope_first)) {
                $participants[$key]->scope_first = $scopes[$participant->scope_first];
            }

            if (!empty($participant->scope_second)) {
                $participants[$key]->scope_second = $scopes[$participant->scope_second];
            }

            if (!empty($participant->scope_third)) {
                $participants[$key]->scope_third = $scopes[$participant->scope_third];
            }

        }

        $dataProviderOverall = new ArrayDataProvider([
            'allModels' => $participants,
        ]);
        $dataProviderOverall->pagination = false;


        return $this->render('departue',
            [
                'dataProviderArrival' => $dataProviderOverall,
                'responsible_array' => $responsible_array
            ]);
    }

    public function actionBydelegations()
    {
        $this->layout = 'admin';


        $dataProviderOrganizations = array();

        $grouped = Participant::find()->with('department')->with('county')->with('capitals')->with('transport')->with('offCapital')->with('offTransport')->all();

        $departments = array();

        foreach ($grouped as $group) {
            $departments[] = $group['department_id'];

        }
        $departments = array_unique($departments);


        $organizationsArr = array();
        $organizationsFinal = array();
        foreach ($departments as $department) {
            $delegation = Participant::find()->where(['department_id' => $department])->with('department')->with('county')->with('capitals')->with('transport')->with('offCapital')->with('offTransport')->with('sex')->all();
            $men = 0;
            $women = 0;
            $beds = array();

            foreach ($delegation as $person) {
//               $arrival = substr($person['departue'], 0, -10);
//                $departue = substr($person['departue'], 0, -10);
                $beds[$person['bed_from']] = isset($beds[$person['bed_from']]) ?
                    $beds[$person['bed_from']] . ', ' . $person['bed_to'] : $person['bed_to'];

                $times[$person['arrival']] = isset($times[$person['arrival']]) ?
                    $times[$person['arrival']] . ', ' . substr($person['departue'], 0, -3) : substr($person['departue'],
                        0,
                        -3);

                $counter[$person['arrival']] = isset($counter[$person['arrival']]) ?
                    $counter[$person['arrival']] + 1 : 1;


                if ($person->sex->name == 'Vyras') {
                    $men++;
                } else {
                    $women++;
                }


            }


            $bedtime = '';
            $timestring = '';

            foreach ($times as $key => $time) {

                $timestring .= $counter[$key] . ' ' . substr($key, 0, -3) . ' - ' . $time . '; ';
            }

            foreach ($beds as $key => $bed) {
                $bedtime .= $key . ' - ' . $bed . '; ';
            }

            $responsible = Participant::find()->where(['department_id' => $department])->andWhere(['responsible' => 1])->with('department')->with('county')->with('capitals')->with('transport')->with('offCapital')->with('offTransport')->with('sex')->one();


            $bedtime = $men . ' Vyr ' . $women . ' Mot' . '  ' . $bedtime;
            $organizationsFinal['delegation'] = $delegation[0]->department->name;
            $organizationsFinal['country'] = $delegation[0]->county->country_name;
            $organizationsFinal['bed'] = $bedtime;

            $organizationsFinal['transport'] = $timestring;
            $organizationsFinal['respname'] = !empty($responsible) ? $responsible['name'] . ' ' . $responsible['surname'] : '';
            $organizationsFinal['respphone'] = !empty($responsible) ? $responsible['responsible_phone'] : '';
            $organizationsFinal['respmail'] = !empty($responsible) ? $responsible['responsible_mail'] : '';


            array_push($organizationsArr, $organizationsFinal);
        }


        $dataProviderOrganizations = new ArrayDataProvider([
            'allModels' => $organizationsArr,

        ]);


        return $this->render('bydelegations',
            [
                'dataProviderGlobal' => $dataProviderOrganizations,
            ]);
    }


    public function actionGlobal()
    {
        $this->layout = 'admin';
        $scopes = Scopes::find()->orderBy(['name' => SORT_ASC])->all();

        $scopeArr = array();
        $country_ids = Participant::find()->all();
        $countryIds = array();
        foreach ($country_ids as $country_id) {

            $name = Countries::find()->where(['id' => $country_id->county_id])->one();
            $countryIds[$name->country_name] = $country_id['county_id'];
        }

        array_unique($countryIds);
        krsort($countryIds);

        $i = 1;
        $allcount = 0;
        $allforeigners = 0;
        $alllithuanians = 0;

        foreach ($countryIds as $key => $countryId) {
            if ($countryId != 127) {
                $allcountries[$key] = 0;
            }
        }

        foreach ($scopes as $scope) {

            $scopeItem = array();
            $countries = array();

            foreach ($countryIds as $key => $countryId) {
                if ($countryId != 127) {
                    $countries[$key] = 0;
                }
            }

            $allParticipant = Participant::find()->where(['scope_first' => $scope->id])->orWhere(['scope_second' => $scope->id])->orWhere(['scope_third' => $scope->id])->all();

            $foreigners = 0;
            $lithuanians = 0;

            foreach ($allParticipant as $participant) {
                if ($participant['county_id'] == 127) {
                    $lithuanians++;
                } else {
                    $foreigners++;
                }
            }
            $count = count($allParticipant);

            foreach ($allParticipant as $oneParticipant) {
                $countyName = Countries::find()->where(['id' => $oneParticipant['county_id']])->one();

                if ($oneParticipant->county_id != 127) {
                    $countries[$countyName['country_name']] += 1;
                    $allcountries[$countyName['country_name']] += 1;
                }

            }
            $countries['Užsieniečiai'] = $foreigners;
            $countries['Lietuviai'] = $lithuanians;
            $countries['Viso'] = $count;
            $countries['Šaka'] = $scope['name'];
            $allcount += $count;
            $scopeItem = array_merge($scopeItem, $countries);
            $allforeigners += $foreigners;
            $alllithuanians += $lithuanians;


            array_push($scopeArr, $scopeItem);
            $i++;

        }
        $allcountries['Užsieniečiai'] = $allforeigners;
        $allcountries['Lietuviai'] = $alllithuanians;
        $allcountries['Viso'] = $allcount;
        $allcountries['Šaka'] = 'VISO';

        array_unshift($scopeArr, $allcountries);

        $dataProviderGlobal = new ArrayDataProvider([
            'allModels' => $scopeArr,
        ]);

        return $this->render('global', compact('dataProviderGlobal', 'dataProviderAccreditations'));
    }

    public function actionOfdelegations()
    {

        $this->layout = 'admin';
        $acreditations = Acreditations::find()->orderBy(['name' => SORT_ASC])->all();

        $providerArr = array();
        $departments = Departments::find()->orderBy(['name' => SORT_ASC])->all();
        $allcount = 0;
        $alllithuanians = 0;
        $allforeigners = 0;
        $alldelegations = array();

        foreach ($departments as $department) {
            $alldelegations[$department['name']] = 0;
        }

        foreach ($acreditations as $acreditation) {


            $providerTmp = array();


            $providerTmp['Akreditacijos'] = $acreditation['name'];
            $providerTmp['Viso'] = 0;
            $all = Participant::find()->where(['accreditation_first' => $acreditation['id']])
                ->orWhere(['accreditation_second' => $acreditation])->all();

            $providerTmp['Lietuviai'] = 0;
            $providerTmp['Užsieniečiai'] = 0;

            foreach ($all as $item) {
                if ($item['county_id'] == 127) {
                    $providerTmp['Lietuviai'] += 1;
                    $alllithuanians += 1;
                } else {
                    $providerTmp['Užsieniečiai'] += 1;
                    $allforeigners += 1;
                }
            }

            $all = count($all);


            $providerTmp['Viso'] = $all;
            $allcount += $all;
            foreach ($departments as $department) {

                $providerTmp[$department['name']] = 0;

                $participants = Participant::find()
                    ->where(['accreditation_first' => $acreditation['id']])
                    ->orWhere(['accreditation_second' => $acreditation['id']])
                    ->andWhere(['department_id' => $department['id']])
                    ->all();
                if (!empty($participants)) {
                    foreach ($participants as $participant) {
                        $country = Countries::find()->where(['id' => $participant['county_id']])->one();
                        $providerTmp[$department['name']] += 1;
                        $alldelegations[$department['name']] += 1;

                    }
                }
            }

            array_push($providerArr, $providerTmp);


        }

        $test = array(
            'Akreditacijos' => 'VISO',
            'Viso' => $allcount,
            'Lietuviai' => $alllithuanians,
            'Užsieniečiai' => $allforeigners
        );
        $newValues = array_merge($test, $alldelegations);

        array_unshift($providerArr, $newValues);

        $dataProviderDelegations = new ArrayDataProvider([
            'allModels' => $providerArr,
        ]);


        return $this->render('ofdelegations', compact('dataProviderDelegations'));
    }

    public function actionOverall()
    {
        $this->layout = 'admin';

        $acreditations = ArrayHelper::map(Acreditations::find()->all(), 'id', 'name');
        $scopes = ArrayHelper::map(Scopes::find()->all(), 'id', 'name');

        $participants = Participant::find()->with('county')->with('department')->with('sex')->with('transport')->with('offTransport')->with('capitals')->with('offCapital')->all();

        $responsible_array = array();
        foreach ($participants as $participant) {
            if (!empty($participant->responsible_phone)) {
                $responsible_array[$participant->department_id]['responsible_phone'] = $participant->responsible_phone;
            }

            if (!empty($participant->responsible_mail)) {
                $responsible_array[$participant->department_id]['responsible_mail'] = $participant->responsible_mail;
            }
        }


        foreach ($participants as $key => $participant) {

            if (!empty($participant->accreditation_first)) {
                $participants[$key]->accreditation_first = $acreditations[$participant->accreditation_first];
            }

            if (!empty($participant->accreditation_second)) {
                $participants[$key]->accreditation_second = $acreditations[$participant->accreditation_second];
            }

            if (!empty($participant->scope_first)) {
                $participants[$key]->scope_first = $scopes[$participant->scope_first];
            }

            if (!empty($participant->scope_second)) {
                $participants[$key]->scope_second = $scopes[$participant->scope_second];
            }

            if (!empty($participant->scope_third)) {
                $participants[$key]->scope_third = $scopes[$participant->scope_third];
            }

        }

        $dataProviderOverall = new ArrayDataProvider([
            'allModels' => $participants,
        ]);
        $dataProviderOverall->pagination = false;

        return $this->render('all', compact('dataProviderOverall', 'responsible_array'));
    }

    public function actionDelegationsandscopes()
    {
        $this->layout = 'admin';

        $finalArray = array();
        $scopes = Scopes::find()->orderBy(['name' => SORT_ASC])->all();
        $delegations = Departments::find()->orderBy(['name' => SORT_ASC])->all();
        $participants = Participant::find()->all();
        $allCount = 0;

        $allForeigners = 0;
        $allLithuanians = 0;

        foreach ($delegations as $delegation) {
            $allArray[$delegation->name] = 0;
        }

        foreach ($scopes as $scope) {
            $tmpArray = array();
            $scopeAll = 0;
            $lithuanians = 0;
            $foreigners = 0;

            $tmpArray['Šaka'] = $scope->name;
            $tmpArray['Viso'] = 0;
            $tmpArray['Lietuviai'] = 0;
            $tmpArray['Užsieniečiai'] = 0;

            foreach ($delegations as $delegation) {
                $tmpArray[$delegation->name] = 0;
            }

            foreach ($delegations as $delegation) {

                foreach ($participants as $participant) {

                    if ($participant->department_id == $delegation->id) {



                        if ($participant->scope_first == $scope->id) {
                            $tmpArray[$delegation->name] += 1;
                            $scopeAll ++;
                            $allArray[$delegation->name] += 1;

                            if($participant->county_id == 127){
                                $lithuanians++;
                                $allLithuanians++;
                            }else{
                                $foreigners++;
                                $allForeigners++;
                            }
                            $allCount++;
                        }

                        if ($participant->scope_second == $scope->id) {
                            $tmpArray[$delegation->name] += 1;
                            $scopeAll ++;
                            $allArray[$delegation->name] += 1;
                            if($participant->county_id == 127){
                                $lithuanians++;
                                $allLithuanians++;
                            }else{
                                $foreigners++;
                                $allForeigners++;
                            }
                            $allCount++;
                        }

                        if ($participant->scope_third == $scope->id) {
                            $tmpArray[$delegation->name] += 1;
                            $scopeAll ++;
                            $allArray[$delegation->name] += 1;
                            if($participant->county_id == 127){
                                $lithuanians++;
                                $allLithuanians++;
                            }else{
                                $foreigners++;
                                $allForeigners++;
                            }
                            $allCount++;
                        }
                    }
                }


            }
            $tmpArray['Viso'] = $scopeAll;
            $tmpArray['Lietuviai'] = $lithuanians;
            $tmpArray['Užsieniečiai'] = $foreigners;

            array_push($finalArray,$tmpArray);

        }

        $mergeArray['Šaka'] = 'VISO';
        $mergeArray['Viso'] = $allCount;
        $mergeArray['Lietuviai'] = $allLithuanians;
        $mergeArray['Užsieniečiai'] = $allForeigners;

        $allArray = array_merge($mergeArray,$allArray);

        array_unshift($finalArray,$allArray);

        $dataProviderDelegationsAndScopes = new ArrayDataProvider([
            'allModels' => $finalArray,
        ]);

        return $this->render('delegationsandscopes', compact('dataProviderDelegationsAndScopes'));
    }

}
