<?php

namespace app\controllers;

use Yii;
use app\models\Settings;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SettingsController implements the CRUD actions for Settings model.
 */
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Settings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'admin';
        $model = Settings::find()->where(['name' => 'activation'])->one();

        if (isset($_POST['Settings'])) {
            $model->value = $_POST['Settings']['value'];
            $model->save();
        }
        return $this->render('index',
            [
                'model' => $model,
            ]);
    }

    /**
     * Displays a single Settings model.
     * @param integer $id
     * @return mixed
     */

}
