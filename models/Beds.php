<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "beds".
 *
 * @property string $bed_from
 * @property string $bed_to
 * @property integer $id
 * @property string $name
 */
class Beds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'beds';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bed_from', 'bed_to'], 'safe'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bed_from' => 'Nakvynė nuo',
            'bed_to' => 'Nakvynė iki',
            'id' => 'ID',
            'name' => 'Pavadinimas',
        ];
    }
}
