<?php

namespace app\models;
use app\models\Countries;
use Yii;

/**
 * This is the model class for table "participant".
 *
 * @property integer $id
 * @property integer $department_id
 * @property integer $county_id
 * @property string $club
 * @property integer $sex_id
 * @property string $name
 * @property string $surname
 * @property integer $age
 * @property integer $accreditation_first
 * @property integer $accreditation_second
 * @property integer $scope_first
 * @property integer $scope_second
 * @property integer $scope_third
 * @property string $specials
 *
 * @property string $bed_from
 * @property string $bed_to
 * @property string $responsible_phone
 * @property string $responsible_mail
 * @property integer $agreed
 *
 * @property Countries $county
 * @property Departments $department
 * @property Sex $sex
 * @property Scopes $scopeFirst
 * @property Scopes $scopeSecond
 * @property Scopes $scopeThird
 */
class Participant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['department_id', 'county_id', 'sex_id', 'name', 'surname', 'age', 'accreditation_first'], 'required',],
            [['id', 'department_id', 'county_id', 'sex_id', 'age', 'scope_first', 'scope_second', 'scope_third'], 'integer'],
            [['bed_from', 'bed_to','responsible','bed','arrival', 'departue'], 'safe'],
            [['agreed'], 'required','requiredValue' => 1,'message'=>'Privalote suktikti su atsakomybėmis.'],
            [['club', 'name', 'surname', 'specials'], 'string', 'max' => 255],
            [['responsible_phone'], 'string', 'max' => 15],
            [['accreditation_first', 'accreditation_second'],'string', 'max' => 10],
            
            [['county_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['county_id' => 'id']],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departments::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['sex_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sex::className(), 'targetAttribute' => ['sex_id' => 'id']],
            [['scope_first'], 'exist', 'skipOnError' => true, 'targetClass' => Scopes::className(), 'targetAttribute' => ['scope_first' => 'id']],
            [['scope_second'], 'exist', 'skipOnError' => true, 'targetClass' => Scopes::className(), 'targetAttribute' => ['scope_second' => 'id']],
            [['scope_third'], 'exist', 'skipOnError' => true, 'targetClass' => Scopes::className(), 'targetAttribute' => ['scope_third' => 'id']],
            [['responsible_mail'], 'email'],
            [['off_capital_id'], 'safe'],
            [['off_transport_id'], 'safe'],
            [['transport_id'],  'safe'],
            [['capitals_id'],  'safe']

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'department_id' => 'Delegacija',
            'county_id' => 'Šalis',
            'club' => 'Klubas',
            'sex_id' => 'Lytis',
            'name' => 'Vardas',
            'surname' => 'Pavardė',
            'age' => 'Amžius',
            'accreditation_first' => 'Akreditacija 1',
            'accreditation_second' => 'Akreditacija 2',
            'scope_first' => 'Sporto šaka 1',
            'scope_second' => 'Sporto šaka 2',
            'scope_third' => 'Sporto šaka 3',
            'specials' => 'Specialūs poreikiai',
            'bed_from' => 'Nakvynė nuo',
            'bed_to' => 'Nakvynė iki',
            'responsible_phone' => 'Atsakingo žmogaus telefonas',
            'responsible_mail' => 'Atsakingo žmogaus elektorinis paštas',
            'agreed' => 'Susipažinęs ir už viską pats atsakau.',
            'responsible'=>'Esu delegacijos atsakingas žmogus',
            'bed'=>'Bus reikalinga nakvynė',
            'transport_id' => 'Atvykimo transportas',
            'capitals_id' => 'Atvykimo miestas',
            'arrival' => 'Atvykimo laikas',
            'departue' => 'Išvykimo laikas',
            'off_transport_id'  => 'Išvykimo transportas',
            'off_capital_id' => 'Išvykimo miestas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounty()
    {
        return $this->hasOne(Countries::className(), ['id' => 'county_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSex()
    {
        return $this->hasOne(Sex::className(), ['id' => 'sex_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScopeFirst()
    {
        return $this->hasOne(Scopes::className(), ['id' => 'scope_first']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScopeSecond()
    {
        return $this->hasOne(Scopes::className(), ['id' => 'scope_second']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScopeThird()
    {
        return $this->hasOne(Scopes::className(), ['id' => 'scope_third']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransport()
    {
        return $this->hasOne(Transport::className(), ['id' => 'transport_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCapitals()
    {
        return $this->hasOne(Capitals::className(), ['id' => 'capitals_id']);
    }

    public function getOffCapital()
    {
        return $this->hasOne(Capitals::className(), ['id' => 'off_capital_id']);
    }

    public function getOffTransport()
    {
        return $this->hasOne(Transport::className(), ['id' => 'off_transport_id']);
    }
}




