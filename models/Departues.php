<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "departues".
 *
 * @property integer $id
 * @property integer $department_id
 * @property integer $transport_id
 * @property integer $capitals_id
 * @property string $arrival
 * @property string $departue
 *
 * @property Departments $department
 * @property Transport $transport
 * @property Capitals $capitals
 */
class Departues extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'departues';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department_id', 'transport_id', 'capitals_id', 'arrival', 'departue'], 'required'],
            [['department_id', 'transport_id', 'capitals_id'], 'integer'],
            [['arrival', 'departue'], 'safe'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departments::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['transport_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transport::className(), 'targetAttribute' => ['transport_id' => 'id']],
            [['capitals_id'], 'exist', 'skipOnError' => true, 'targetClass' => Capitals::className(), 'targetAttribute' => ['capitals_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'department_id' => 'Delegacija',
            'transport_id' => 'Transportas',
            'capitals_id' => 'Miestas',
            'arrival' => 'Atvykimas',
            'departue' => 'Išvykimas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransport()
    {
        return $this->hasOne(Transport::className(), ['id' => 'transport_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCapitals()
    {
        return $this->hasOne(Capitals::className(), ['id' => 'capitals_id']);
    }
}
