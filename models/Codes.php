<?php

namespace app\models;

use Yii;
use app\models\User;

/**
 * This is the model class for table "codes".
 *
 * @property integer $id
 * @property string $code
 * @property integer $department_id
 *
 * @property Departments $department
 */
class Codes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'codes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['department_id'], 'integer'],
            [['code'], 'string', 'max' => 255],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departments::className(), 'targetAttribute' => ['department_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Kodas',
            'department_id' => 'Bendruomenė',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }

    public function login()
    {
        $model = new User;
        $model ->username = 'tttt';
        return Yii::$app->user->login($model );
    }


}
