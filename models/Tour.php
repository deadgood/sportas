<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tour".
 *
 * @property integer $id
 * @property string $name
 * @property string $arrival_from
 * @property string $arrival_to
 */
class Tour extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tour';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'arrival_from', 'arrival_to'], 'required'],
            [['arrival_from', 'arrival_to'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Pavadinimas',
            'arrival_from' => 'Atvykimas nuo',
            'arrival_to' => 'Atvykimas iki',
        ];
    }
}
