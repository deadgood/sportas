<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "scopes".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Participant[] $participants
 * @property Participant[] $participants0
 * @property Participant[] $participants1
 */
class Scopes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scopes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Pavadinimas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipants()
    {
        return $this->hasMany(Participant::className(), ['scope_first' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipants0()
    {
        return $this->hasMany(Participant::className(), ['scope_second' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipants1()
    {
        return $this->hasMany(Participant::className(), ['scope_third' => 'id']);
    }
}
