<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "acreditations".
 *
 * @property integer $id
 * @property string $name
 * @property integer $dependency
 */
class Acreditations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'acreditations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dependency'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Pavadinimas',
            'dependency' => 'Ar turi sporto šakų?',
        ];
    }
}
