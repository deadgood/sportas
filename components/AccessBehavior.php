<?php
/**
 * Created by Eimantas J.
 * Project: leidiniai.vgtu.lt
 * Date: 2015-08-03 12:49
 * Version: v0.1
 */
namespace app\components;


use yii\base\Behavior;
use yii\console\Controller;
use yii\helpers\Url;
use Yii;

/**
 * Redirects all users to login page if not logged in
 *
 * Class AccessBehavior
 * @package app\components
 * @author  Artem Voitko <r3verser@gmail.com>
 */
class AccessBehavior extends Behavior
{
    /**
     * Subscribe for events
     * @return array
     */
    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'beforeAction'
        ];
    }

    /**
     * On event callback
     */
    public function beforeAction()
    {


        $admin = array('admin', 'department', 'codes', 'beds', 'tour', 'capitals','acreditations','countries','departues','scopes','user','adminp','tables','transport','settings');

        if (!in_array(Yii::$app->controller->id, $admin) && Yii::$app->controller->action->id != 'login') {


            if (\Yii::$app->getUser()->isGuest) {

                \Yii::$app->getResponse()->redirect(\Yii::$app->getUser()->loginUrl);

            } else {
                if (Yii::$app->user->Identity->admin == 1) {
                    return Yii::$app->getResponse()->redirect(['/admin/index']);
                }
            }
        } elseif (in_array(Yii::$app->controller->id, $admin) && Yii::$app->controller->action->id != 'login') {


            if (\Yii::$app->getUser()->isGuest || Yii::$app->user->Identity->admin != 1) {
                return Yii::$app->getResponse()->redirect(['/admin/login']);

            }

        }

    }
}