<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=sportas',
    'username' => 'homestead',
    'password' => 'secret',
    'charset' => 'utf8',
];
