<?php

use yii\db\Migration;

/**
 * Handles the creation for table `codes`.
 */
class m160725_135843_create_codes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('codes', [
            'id' => $this->primaryKey(),
            'code' => $this->string(),
            'department_id'=> $this->integer()
        ]);

        $this->addForeignKey(
            'codes_ibfk_1',
            'codes',
            'department_id',
            'departaments',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-post-category_id',
            'codes'
        );
        $this->dropTable('codes');
    }
}
