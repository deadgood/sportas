<?php

use yii\db\Migration;

/**
 * Handles the creation for table `countries`.
 */
class m160725_124358_create_countries_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('countries', [
            'id' => $this->primaryKey(),
            'country_code' => $this->string(),
            'country_name'=> $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('countries');
    }
}
