<?php

use yii\db\Migration;

/**
 * Handles the creation for table `departues`.
 */
class m160725_135739_create_departues_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('departues', [
            'id' => $this->primaryKey(),
            'department_id' => $this->integer(),
            'transport_id' => $this->integer(),
            'capitals_id' => $this->integer(),
            'arrival' => $this->dateTime(),
            'departue' => $this->dateTime()
        ]);

        $this->addForeignKey(
            'departues_ibfk_1',
            'departues',
            'department_id',
            'departaments',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'departues_ibfk_2',
            'departues',
            'transport_id',
            'transport',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'departues_ibfk_3',
            'departues',
            'capitals_id',
            'capitals',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'departues_ibfk_2',
            'departues'
        );
        $this->dropForeignKey(
            'departues_ibfk_2',
            'departues'
        );
        $this->dropForeignKey(
            'departues_ibfk_3',
            'departues'
        );
        $this->dropTable('departues');
    }
}
