<?php

use yii\db\Migration;

/**
 * Handles the creation for table `participant`.
 */
class m160725_135629_create_participant_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('participant', [
            'id' => $this->primaryKey(),
            'department_id' => $this->integer(),
            'county_id' => $this->integer(),
            'club' => $this->string(),
            'sex_id' => $this->integer(),
            'name' => $this->string(),
            'surname' => $this->string(),
            'age'=> $this->integer(),
            'accreditation_first'=>$this->integer(),
            'accreditation_second' =>$this->integer(),
            'scope_first'=>$this->integer(),
            'scope_second' => $this->integer(),
            'scope_third'=> $this->integer(),
            'specials' => $this->string(),
            'bed_from'=>$this->date(),
            'bed_to'=>$this->date(),
            'responsible_phone'=> $this->string(),
            'responsible_mail'=>$this->string(),
            'agreed'=> $this->integer(),
            'responsible' => $this->integer(),
            'bed' => $this->integer(),
            'transport_id' => $this->integer(),
            'capitals_id' => $this->integer(),
            'arrival' => $this->dateTime(),
            'departue'=> $this->dateTime(),
            'off_transport_id'=>$this->integer(),
            'off_capital_id' => $this->integer(),

        ]);

        $this->addForeignKey(
            'participant_ibfk_1',
            'participant',
            'department_id',
            'departaments',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'participant_ibfk_2',
            'participant',
            'county_id',
            'countries',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'participant_ibfk_3',
            'participant',
            'sex_id',
            'sex',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'participant_ibfk_4',
            'participant',
            'accreditation_first',
            'acreditations',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'participant_ibfk_5',
            'participant',
            'accreditation_second',
            'acreditations',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'participant_ibfk_6',
            'participant',
            'scope_first',
            'scopes',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'participant_ibfk_7',
            'participant',
            'transport_id',
            'transport',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'participant_ibfk_8',
            'participant',
            'capitals_id',
            'capitals',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'participant_ibfk_9',
            'participant',
            'capitals_id',
            'capitals',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'participant_ibfk_10',
            'participant',
            'off_transport_id',
            'transport',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'participant_ibfk_11',
            'participant',
            'off_capital_id',
            'transport',
            'id',
            'CASCADE'
        );


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'participant_ibfk_1',
            'participant'
        );
        $this->dropForeignKey(
            'participant_ibfk_2',
            'participant'
        );
        $this->dropForeignKey(
            'participant_ibfk_3',
            'participant'
        );
        $this->dropForeignKey(
            'participant_ibfk_4',
            'participant'
        );
        $this->dropForeignKey(
            'participant_ibfk_5',
            'participant'
        );
        $this->dropForeignKey(
            'participant_ibfk_6',
            'participant'
        );
        $this->dropForeignKey(
            'participant_ibfk_7',
            'participant'
        );
        $this->dropForeignKey(
            'participant_ibfk_8',
            'participant'
        );
        $this->dropForeignKey(
            'participant_ibfk_9',
            'participant'
        );
        $this->dropForeignKey(
            'participant_ibfk_10',
            'participant'
        );
        $this->dropForeignKey(
            'participant_ibfk_11',
            'participant'
        );

        $this->dropTable('participant');
    }
}
