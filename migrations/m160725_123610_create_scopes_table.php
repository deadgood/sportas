<?php

use yii\db\Migration;

/**
 * Handles the creation for table `scopes`.
 */
class m160725_123610_create_scopes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('scopes', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('scopes');
    }
}
