<?php

use yii\db\Migration;

/**
 * Handles the creation for table `users`.
 */
class m160725_135459_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'department_id'=>$this->integer(),
            'username'=>$this->string(),
            'password'=>$this->string(),
            'name'=>$this->string(),
            'surname'=>$this->string(),
            'userid'=>$this->integer(),
            'admin' => $this->integer(),
            'auth_key'=>$this->string(),
            'active'=>$this->integer(),
        ]);
        $this->addForeignKey(
            'users_ibfk_1',
            'users',
            'department_id',
            'departaments',
            'id',
            'CASCADE'
        );


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'users_ibfk_1',
            'users'
        );
        $this->dropTable('users');
    }
}
