<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tour`.
 */
class m160725_123405_create_tour_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tour', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
            'arrival_from'=>$this->date(),
            'arrival_to'=>$this->date(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tour');
    }
}
