<?php

use yii\db\Migration;

/**
 * Handles the creation for table `beds`.
 */
class m160725_124840_create_beds_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('beds', [
            'id' => $this->primaryKey(),
            'bed_from' => $this->date(),
            'bed_to' => $this->date(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('beds');
    }
}
