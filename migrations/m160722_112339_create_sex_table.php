<?php

use yii\db\Migration;

/**
 * Handles the creation for table `sex`.
 */
class m160722_112339_create_sex_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sex', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('sex');
    }
}
