<?php

use yii\db\Migration;

/**
 * Handles the creation for table `transport`.
 */
class m160725_123107_create_transport_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('transport', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('transport');
    }
}
