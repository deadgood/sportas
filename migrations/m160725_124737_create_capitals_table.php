<?php

use yii\db\Migration;

/**
 * Handles the creation for table `capitals`.
 */
class m160725_124737_create_capitals_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('capitals', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('capitals');
    }
}
