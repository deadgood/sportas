<?php

use yii\db\Migration;

/**
 * Handles the creation for table `acreditations`.
 */
class m160722_113003_create_acreditations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('acreditations', [
            'id' => $this->primaryKey(),
            'dependency' =>$this->integer(),
            'name'=>$this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('acreditations');
    }
}
