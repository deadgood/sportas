<?php

use yii\db\Migration;

/**
 * Handles the creation for table `departaments`.
 */
class m160725_135402_create_departaments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('departaments', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('departaments');
    }
}
