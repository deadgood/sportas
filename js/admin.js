/**
 * Created by Matas on 2016-07-02.
 */
function tables($item) {

    $.ajax({
        url: '/adminp/main',
        type: "POST",
        data: {id: $item.value},
        success: function (data) {
            $(".participant-index").replaceWith(data);

        }

    });
}
$(document).ready(function () {

    if (window.location.search.indexOf('department') == 1) {
        var id = $("#get").val();

        $.ajax({
            url: '/adminp/main',
            type: "POST",
            data: {id: id},
            success: function (data) {
                $(".participant-index").replaceWith(data);

            }

        });


    }


    $(window).load(function () {

        $('#ex1').slider({
            formatter: function (value) {
                return 'Current value: ' + value;
            }
        });
        $("#ex5").slider();

    });

    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = 'https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X', 'auto');
    ga('send', 'pageview');


    var curtitle = $('.pageheader').children().children('b').text().trim();

    $("#navigation").children('li').each(function () {
        var curmenu = $(this).children().children('span').text().trim();


        if (curmenu == curtitle) {

            $(this).addClass('active');
        }
    });

});