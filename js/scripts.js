/**
 * Created by Matas on 2016-06-30.
 */
function acreditations() {
    var div = $("#optional_acreditation");
    var plius = $("#acrplus");
    if (div.is(":visible")) {
        div.hide();
        $("#optional_acreditation option[value='']").attr('selected', true);
        plius.show();
        $("#second_acr").val('');

    } else {
        plius.hide();
        div.show();
    }
}

function scopesShow(select) {

    var check_manager = $('#first_acr option:selected').text();
    var check_manager_second = $('#second_acr option:selected').text();
    if (check_manager == 'Delegacijos vadovas' || check_manager_second == 'Delegacijos vadovas') {
        var block = $("#responsible_block");
        block.show();

    } else {
        if (check_manager == 'Delegacijos vadovas' || check_manager_second == 'Delegacijos vadovas') {

        } else {
            $("#responsible_block").hide();
        }

        var arr = select.value.split('_');
        var id = arr[0];
        var checker = arr[1];
        if (select.id == "first_acr") {

            if (checker == 1) {
                $("#scopes").show();

            } else {
                $("#scopes").hide();
                $("#scopes").children("div").each(function () {
                    var scopechecks = $(this).children().children("select").val('');
                });
            }
        } else {
            if (checker == 1) {
                $("#scopes").show();

            }
        }
        //scopes
    }
}

function scopes(id) {
    //console.log();
    if (id.id == "first_scope") {

        $("#" + id.id).parent().parent().parent().next("div").show();
        $("#" + id.id).hide();


    }
    else if (id.id == "second_scope") {
        $("#" + id.id).parent().next("div").show();
        $("#" + id.id).parent().next("div").children("a").hide();
        $("#" + id.id).hide();
        $("#" + id.id).next("div").children().children().hide();
    }
    else {
        $("#" + id.id).parent().next("div").show();
        $("#" + id.id).hide();
        $("#" + id.id).next("div").children().children().hide();
    }
}
function scopesdel(id) {
    if (id.id == "first_delete") {
        $("#" + id.id).parent().parent().prev().prev().children('select').val('');
        $("#" + id.id).parent().parent().parent().hide();
        $("#" + id.id).parent().parent().parent().prev().children().children().children().show();

    } else {
        // $("#"+id.id).parent().parent().parent().hide();

        $("#" + id.id).parent().parent().parent().prev().children("a").show();
        $("#" + id.id).parent().parent().parent().prev().children(".row").children().children().show();
        $("#" + id.id).parent().parent().parent().hide();

    }
}

function bedtime(item) {


    var county = $("#country_input").val();
    if ($("#" + item.id).is(':checked')) {

        $.ajax({
            url: '/participant/dates',
            type: "POST",
            data: {county: county},
            success: function (data) {
                $("#bed_block").replaceWith(data);

            }

        });


    } else {

        $("#bed_block").html('');

    }
}

function departue(item) {
    var block = $("#departue_block");
    var arrival_block = $(".arrival-block");
    var departure_block = $(".departure_block");
    var arrival_button = $('#arrival_custom').parent();
    var departure_button = $('#departure_custom').parent();

    if (item.value != 127) {
        block.show();
        departure_block.show();
        arrival_block.show();
        arrival_button.show();
        departure_button.show();
    } else {
        $('#participant-departue').val('');
        $('#participant-arrival').val('');
        $('#participant-transport_id').val('');
        $('#participant-capitals_id').val('');
        block.hide();
        arrival_button.hide();
        departure_button.hide();
    }
}

function showArrivals() {
    var selectedCounty = $("#country_input option:selected").val();
    var block = $(".arrival-block");
    var block_other = $(".departure_block");
    if (selectedCounty.length == 0 || selectedCounty == 127) {
        block.hide();
    } else {
        if (block.is(":visible")) {
            block.hide();
            $('#participant-transport_id').val('');
            $('#participant-capitals_id').val('');
            $('#participant-arrival').val('');
        } else {
            block.show();
        }
    }
}

function showDeparture() {
    var selectedCounty = $("#country_input option:selected").val();
    var block = $(".departure_block");
    var block_other = $(".arrival-block");
    if (selectedCounty.length == 0 || selectedCounty == 127) {
        block.hide();
    } else {
        if (block.is(":visible")) {
            block.hide();
            $('#participant-off_transport_id').val('');
            $('#participant-off_capital_id').val('');
            $('#participant-departue').val('');
        } else {
            block.show();
        }
    }
}

$(document).ready(function () {
    var responsibleblock = $('#responsible_block');
    var responsiblecheck = responsibleblock.children().children("input").val();
    var departueblock = $('#departue_block');

    var bedblock = $('#bed_block');
    var bedcheck = $('#bed_date').val();
    var scopesblock = $('#scopes');
    var scopescheck = $('#scope_1').val();
    var acreditationopt = $("#optional_acreditation");
    var acreditationcheck = $("#second_acr").val();
    var selectedCounty = $("#country_input option:selected").val();
    var arrival_empty = $('#participant-transport_id').val();
    var departure_empty = $('#participant-off_transport_id').val();


    if (selectedCounty != 127 && selectedCounty.length > 0) {
        departueblock.show()
    }

    if (responsiblecheck) {
        responsibleblock.show();
    }

    if (acreditationcheck) {
        acreditationopt.show();
    }

    if (bedcheck) {
        bedblock.show();
    }

    if (scopescheck) {
        scopesblock.show();
    }

    $("#scopes").children("div").each(function () {

        var scopechecks = $(this).children().children("select").val();

        if (scopechecks) {
            $(this).show();
        }
    });
    var scope1 = $(".scope_1");
    var scope2 = $(".scope_2");
    var scope3 = $(".scope_3");

    if (scope3.is(":visible")) {
        $("#first_delete").hide();
        $("#second_scope").hide();
        $("#first_scope").hide();
        $("#third_scope").hide();
    }
    if (scope2.is(":visible") && !scope3.is(":visible")) {
        $("#first_scope").hide();
    }

    if ($('#optional_acreditation').is(":visible")) {
        $("#acrplus").hide();
    }

    if ($("#participant-bed").is(':checked')) {

        var county = $("#country_input").val();
        var iditem = $('#item_id').val();

        $.ajax({
            url: '/participant/dates',
            type: "POST",
            data: {
                county: county, iditem: iditem

            },
            success: function (data) {
                $("#bed_block").replaceWith(data);

            }

        });
    }

    if(arrival_empty.length == 0 && selectedCounty.length > 0){
        $('#arrival_custom').prop('checked', true);
        $('.arrival-block').hide();
    }

    if(departure_empty.length == 0 && selectedCounty.length > 0){
        $('#departure_custom').prop('checked', true);
        $('.departure_block').hide();
    }

});

